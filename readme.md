<h1>GenGenie</h1>

<ol>
    <li><a href="#li1">What is GenGenie?</a></li>
    <li><a href="#li2">What can it do?</a></li>
    <li><a href="#li3">How can I add the public bot to my Discord server?</a></li>
    <li><a href="#li4">Setup (for developers)</a></li>
</ol>

<div style="pad-bottom:20px">
<h2 id="li1">What is GenGenie?</h2>
GenGenie is a discord bot for the RPG system Genesys.  
</div>

<div style="pad-bottom:20px">
<h2 id="li2">What can it do?</h2>
GenGenie currently has the following commands (parameters in [brackets] are optional):<br/>
<table>
<tr>
    <th>command</th>
    <th>usage</th>
    <th>description</th>
</tr>
<tr>
    <td>help</td>
    <td>help [commandName]</td>
    <td>`help` without an optional parameter will list all commands and usages.  With an optional parameter
     it will give an explanation for that command.</td>
</tr>
<tr>
    <td>roll</td>
    <td>roll [dicepool or number]</td>
    <td>
    Roll can take several types of input:<br/>
    To roll a sided die, like a D20, use roll 20.<br/>
    To roll a Genesys die, add any number of the following:<br/>
    y  : Proficiency (Yellow)<br/>
    g  : Ability (Green)<br/>
    b  : Bonus (Blue)<br/>
    r  : Challenge (Red)<br/>
    p  : Difficulty (Purple)<br/>
    k  : Setback (Black)<br/>
    To add automatic results, add any of the following:<br/>
    s  : Success<br/>
    a  : Advantage<br/>
    f  : Failure<br/>
    t  : Threat<br/>
    </td>
</tr>
<tr>
    <td>getDice</td>
    <td>getdice poolname</td>
    <td>
    getdice allows you to retrieve a standard set of dice by poolname.<br/>
    Example: getdice brawl<br/>
    Dice are user specific, if two users set a key of `ranged` they will not conflict.<br/>
    </td>
</tr>
<tr>
    <td>setDice</td>
    <td>setdice poolname dice</td>
    <td>
    setdice allows you to assign a standard set of dice to a poolname.<br/>
    Example: setdice brawl yyyg<br/>
    Setting dice is user specific, if two users set a key of `ranged` they will not conflict.<br/>
    </td>
</tr>
<tr>
    <td>useDice</td>
    <td>usedice poolname [additional dice]</td>
    <td>
    usedice allows you to roll a dice pool created using setdice, plus additional dice.<br/>
    Example: usedice brawl pp<br/>
    Dice are user specific, if two users have a key of `ranged` they will not conflict.<br/>
    </td>
</tr>
<tr>
    <td>invite</td>
    <td>invite</td>
    <td>Generates an invite link for the current bot.</td>
</tr>
<tr>
    <td>init</td>
    <td>init [subcommand]</td>
    <td>
    init has several subfunctions for managing turn order.<br/>
    `init addme` will add you as a PC to the turn order.<br/>
    `init addnpc npcId coolpool vigilancePool` will add an NPC to the turn order.<br/>
    Example: `init addnpc GoldDragon yyyg ygg`.<br/>
    `init addnpc clearall` will clear the players and npcs from the turn order.<br/>
    `init addnpc clearplayers` will clear the players from the turn order.;<br/>
    `init addnpc clearnpcs` will clear the npcs from the turn order.<br/>
    `init report` will return a report on the turn order.<br/>
    `init roll playerPool npcPool` will roll the configured initiative and print the turn order.<br/>
    Example: `init roll cool vigilance` will roll the players' cool and the NPCs' vigilance.<br/>
    </td>
</tr>
<tr>
    <td>crit</td>
    <td>crit</td>
    <td>
    `crit me [+roll]` will crit the player, rolling (1d100+[+roll]+(existingCrits*10)) and returning the appropriate critical injury.<br/>
    `crit npc npcId [+roll]` will crit the specified npc, rolling (1d100+[+roll]+(existingCrits*10)) and returning the appropriate critical injury.<br/>
    `crit get [npcId]` will get the list of critical injuries for the given character.<br/>
    `crit clearall [npcId]` will clear the list of critical injuries for the given character.<br/>
    `crit clear index [npcId]` will clear the critical injury at the given index (1, 2, etc) for the given character.<br/>
    `crit clearoldest [npcId]` will clear the oldest critical injury for the given character.<br/>
    `crit longrest index` will attempt to heal a player critical injury at the given index (1, 2, etc) using their `resilience` dice pool.<br/>
    </td>
</tr>
<tr>
    <td>dump</td>
    <td>dump dumpType</td>
    <td>
    The dump command is used for printing troubleshooting information. Currently implemented dumps:<br/>
    reactions - dumps all configured reaction images to channel for inspection
    </td>
</tr>
</table>
</div>

<div style="pad-bottom:20px">
<h2 id="li3">How can I add the public bot to my discord server?</h2>
Follow <a href="https://discordapp.com/api/oauth2/authorize?client_id=669709426537660426&scope=bot&permissions=272448" target="_blank">this link</a> to the Discord API site, sign in, and select which server you want to add the bot to.<br/><br/>
You'll need administrative permissions.  Once added, use ".gg help" to get started.
</div>

<div style="pad-bottom:20px">
<h2 id="li4">Setup (for developers)</h2>
    Checkout the <a href="#" target="_blank">develop</a> branch of the code.  This is a Java 8+ project.  Any and all PRs must be done against develop.
    <br/><br/>
    Only thing you should really need to do is rename src/main/resources/config.properties.sample to config.properties and add your bot/redis values.
    <br/><br/>
    You don't <i>need</i> a redis server for all commands, but a lot of the cooler functionality will make use of it.
    <br/><br/>
    If you don't know where to get a bot clientid/token, check out <a href="https://www.howtogeek.com/364225/how-to-make-your-own-discord-bot/" href="_blank">this article</a>.
</div>