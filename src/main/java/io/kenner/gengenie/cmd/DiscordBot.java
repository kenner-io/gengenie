package io.kenner.gengenie.cmd;

import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.MessageChannel;
import io.kenner.gengenie.input.Parser;
import io.kenner.gengenie.redis.Redis;

import java.io.IOException;
import java.util.List;
import java.util.Properties;


public class DiscordBot {
    private static Properties configuration;
    private static final String botToken;
    private static final String clientId;
    private static final String permissions;

    static {
        System.setProperty("javax.xml.bind.JAXBContextFactory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        System.out.println("Loading configuration...");
        configuration = new Properties();
        try {
            configuration.load(DiscordBot.class.getResourceAsStream("/config.properties"));
        } catch (IOException e) {
            System.out.println("Error loading configuration: " + e.getMessage());
            e.printStackTrace();
            System.exit(0);
        }
        botToken = configuration.getProperty("bot.token");
        clientId = configuration.getProperty("bot.clientid");
        permissions = configuration.getProperty("bot.permissions");
    }

    public static void main(String[] args) {
        DiscordClient client = new DiscordClientBuilder(botToken).build();

        if(Redis.getConnectionStatus()) {
            try {
                Redis.set("ping", "Successful Redis Ping");
                System.out.println(Redis.get("ping"));
                Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                    public void run() {
                        Redis.Disconnect();
                    }
                }, "Shutdown-thread"));
            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }

        client.getEventDispatcher().on(MessageCreateEvent.class)
            .subscribe(event -> {
                try {
                    // ignore bot traffic
                    String req = event.getMessage().getContent().get();
                    if (!event.getMessage().getAuthor().get().isBot()) {
                        List<String> resp = Parser.Process(req, true, event);
                        if (!resp.isEmpty()) {
                            MessageChannel channel = (MessageChannel) client.getChannelById(event.getMessage().getChannelId()).block();
                            for (String msg : resp) {
                                channel.createMessage(msg).block();
                            }
                        }
                    }
                } catch (Exception e) {
                    // do nothing
                    System.out.println(e.toString());
                }
            });

        client.login().block();
    }

    /*
        Getters/Setters
     */
    public static String getClientId() {
        return clientId;
    }

    public static String getPermissions() {
        return permissions;
    }
}
