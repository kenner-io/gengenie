package io.kenner.gengenie.cmd;

import io.kenner.gengenie.input.Parser;

import java.util.List;
import java.util.Scanner;

public class Terminal {

    public static void main(String[] args) {
        String input = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("GenGenie Terminal, enter input ('q' to quit):");
        while (!input.equals("q")) {
            input = scanner.nextLine();
            List<String> output = Parser.Process(input, false, null);
            for (String s : output) {
                System.out.println(s);
            }
        }
    }
}
