package io.kenner.gengenie.redis;


import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

public class Redis {
    private static final String redisUrlWithAuth = "redis://%s@%s:%s";
    private static final String redisUrlNoAuth = "redis://%s:%s";
    private static RedisClient client;
    private static StatefulRedisConnection<String, String> connection;
    private static RedisCommands<String, String> cmd;
    private static final String host;
    private static final String port;
    private static final String token;
    private static final String connUrl;
    private static final AtomicBoolean hasConnection = new AtomicBoolean(false);

    static {
        System.out.println("Loading redis configuration...");
        Properties configuration = new Properties();
        try {
            configuration.load(Redis.class.getResourceAsStream("/config.properties"));
        } catch (IOException e) {
            System.out.println("Error loading redis configuration: " + e.getMessage());
            e.printStackTrace();
            System.exit(0);
        }
        host = configuration.getProperty("redis.host");
        port = configuration.getProperty("redis.port");
        token = configuration.getProperty("redis.password");
        if(token.isEmpty()) {
            connUrl = String.format(redisUrlNoAuth, host, port);
        } else {
            connUrl = String.format(redisUrlWithAuth, token, host, port);
        }

        System.out.println("Creating redis client");
        try {
            client = RedisClient.create(connUrl);
            connection = client.connect();
            cmd = connection.sync();
            System.out.println("Connected to redis server");
            hasConnection.set(true);
        } catch (Exception e) {
            System.out.println("Unable to connect to redis server: ");
            e.printStackTrace();
        }
    }

    public static void set(String key, String value) throws Exception {
        cmd.set(key, value);
    }

    public static String get(String key) throws Exception {
        return cmd.get(key);
    }

    public static boolean getConnectionStatus() {
        return hasConnection.get();
    }

    public static void Disconnect() {
        hasConnection.set(false);
        try {
            System.out.println("Closing redis connections");
            connection.close();
            client.shutdown();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}
