package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Reactions;

public class AbilityDie extends Die implements IDie {

    public AbilityDie() {
        sides = 8;
    }

    @Override
    public Result roll() {
        return ResultSets.GetAbilityResult(Die.RollD(sides));
    }

    @Override
    public String getReaction(int rawResult) {
        switch (rawResult) {
            case 0:
                return Reactions.ABILITY_GIF;
            case 1:
                return Reactions.ABILITY_BLANK;
            case 2:
            case 3:
                return Reactions.ABILITY_S;
            case 4:
                return Reactions.ABILITY_SS;
            case 5:
            case 6:
                return Reactions.ABILITY_A;
            case 7:
                return Reactions.ABILITY_SA;
            case 8:
                return Reactions.ABILITY_AA;
            default:
                return "";
        }
    }

}
