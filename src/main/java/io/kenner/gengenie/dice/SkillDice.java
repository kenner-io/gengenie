package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Parser;
import io.kenner.gengenie.input.Reactions;

import java.util.ArrayList;
import java.util.List;

/**
 * SkillDice is a collection of positive dice and utility functions to make use of them.
 *
 * SkillDice is meant to hold the (typically positive) dice that are used as part of every roll.
 * Negative dice are typically situational/variable and are instead handled as function parameters.  This is not enforced
 * via interface as there can be exceptions.  For example, heavy armor can add setback dice to all stealth checks.
 *
 * For example, if a Character has Ranged skill of yygg, those dice will be used for all ranged attacks.  However,
 * the non-skill dice may change (2d for short range, 3d for medium range, 1b for aim maneuvers, etc.)
 */
public class SkillDice {
    private final List<IDie> dice = new ArrayList<IDie>();
    ArrayList<String> autoResultReactions = new ArrayList<String>();
    private int autoS = 0, autoA = 0, autoF = 0, autoT = 0;
    public static final String ERR_NODICE = "No dice to roll!";
    private static final String discordIdFormat = "<@%s>: ";

    public SkillDice(){
        // default constructor needs to be specified, else it would be inaccessible
    }

    /**
     * Creates and adds n dice of each type to the skill dice pool. For an empty pool use the default constructor.
     * @param proficiency
     * @param ability
     * @param bonus
     * @param challenge
     * @param difficulty
     * @param setback
     */
    public SkillDice(int proficiency, int ability, int bonus, int challenge, int difficulty, int setback){
        for(int i = 0; i < proficiency; i++) {
            dice.add(new ProficiencyDie());
        }

        for(int i = 0; i < ability; i++) {
            dice.add(new AbilityDie());
        }

        for(int i = 0; i < bonus; i++) {
            dice.add(new BonusDie());
        }

        for(int i = 0; i < challenge; i++) {
            dice.add(new ChallengeDie());
        }

        for(int i = 0; i < difficulty; i++) {
            dice.add(new DifficultyDie());
        }

        for(int i = 0; i < setback; i++) {
            dice.add(new SetbackDie());
        }
    }

    /**
     * Wraps rollDiceWith passing in no additional dice.
     * @return
     */
    public ResultReport roll() {
        return this.rollDiceWith(0,0,0,0,0,0);
    }

    /**
     * Rolls the base dice pool with extra dice
     * @param addlProficiency
     * @param addlAbility
     * @param addlBonus
     * @param addlChallenge
     * @param addlDifficulty
     * @param addlSetback
     * @return
     */
    public ResultReport rollDiceWith(int addlProficiency, int addlAbility, int addlBonus, int addlChallenge, int addlDifficulty, int addlSetback) {
        // compile all the dice needed to make the roll
        ArrayList<IDie> dicePool = new ArrayList<IDie>();
        dicePool.addAll(this.dice);

        for(int i = 0; i < addlProficiency; i++) {
            dice.add(new ProficiencyDie());
        }

        for(int i = 0; i < addlAbility; i++) {
            dice.add(new AbilityDie());
        }

        for(int i = 0; i < addlBonus; i++) {
            dice.add(new BonusDie());
        }

        for(int i = 0; i < addlChallenge; i++) {
            dice.add(new ChallengeDie());
        }

        for(int i = 0; i < addlDifficulty; i++) {
            dice.add(new DifficultyDie());
        }

        for(int i = 0; i < addlSetback; i++) {
            dice.add(new SetbackDie());
        }

        // roll the dice
        ArrayList<Result> results = new ArrayList<Result>();
        for(IDie die : dicePool) {
            results.add(die.roll());
        }

        // compile results into a single result
        Result accumResult = Result.AccumulateResults(results);

        // add auto results to accumulation
        accumResult.successes += autoS;
        accumResult.failures += autoF;
        accumResult.advantage += autoA;
        accumResult.threat += autoF;

        // Get Net Result
        Result netResult = accumResult.getNetResult();

        // generate result dice reactions and add them to ResultReport
        ArrayList<String> reactionDice = new ArrayList<String>();
        for(int i = 0; i < results.size(); i++){
            reactionDice.add(dice.get(i).getReaction(results.get(i).getRaw()));
        }
        // add auto result dice to reactionDice
        reactionDice.addAll(autoResultReactions);

        // Return the consolidated results of the dice roll
        return new ResultReport(results, accumResult, netResult, reactionDice);
    }

    /**
     * Parse takes an input string of Genesys dice (such as yyyg) and constructs a diceBag from it.
     * @param dice
     * @return
     */
    public static SkillDice parse(String dice) {
        SkillDice diceBag = new SkillDice();

        // count automatic results and factor them into the accum result before netting.
        for (char c : dice.toCharArray()) {
            switch (c) {
                case Parser.PROFICIENCY_DIE:
                    diceBag.getDice().add(new ProficiencyDie());
                    break;
                case Parser.CHALLENGE_DIE:
                    diceBag.getDice().add(new ChallengeDie());
                    break;
                case Parser.ABILITY_DIE:
                    diceBag.getDice().add(new AbilityDie());
                    break;
                case Parser.DIFFICULTY_DIE:
                    diceBag.getDice().add(new DifficultyDie());
                    break;
                case Parser.BONUS_DIE:
                    diceBag.getDice().add(new BonusDie());
                    break;
                case Parser.SETBACK_DIE:
                    diceBag.getDice().add(new SetbackDie());
                    break;
                case Parser.AUTO_SUCCESS:
                    diceBag.autoS++;
                    diceBag.getAutoResultReactions().add(Reactions.SUCCESS);
                    break;
                case Parser.AUTO_ADVANTAGE:
                    diceBag.autoA++;
                    diceBag.getAutoResultReactions().add(Reactions.ADVANTAGE);
                    break;
                case Parser.AUTO_FAILURE:
                    diceBag.autoF++;
                    diceBag.getAutoResultReactions().add(Reactions.FAILURE);
                    break;
                case Parser.AUTO_THREAT:
                    diceBag.autoT++;
                    diceBag.getAutoResultReactions().add(Reactions.THREAT);
                    break;
            }
        }
        return diceBag;
    }

    /**
     * getBlankDice returns the raw, unrolled dice pool.
     * @return
     */
    public String getBlankDice() {
        StringBuilder res = new StringBuilder();
        for(IDie die : this.getDice()) {
            res.append(die.getReaction(1));
        }
        for(String autoR : this.getAutoResultReactions()) {
            res.append(autoR);
        }

        return res.toString();
    }

    /**
     * rollWithOutput rolls a dice pool and returns a list of parser-compliant message responses.
     * @param isDiscord
     * @param discordId
     * @return
     */
    public List<String> rollWithOutput(boolean isDiscord, String discordId) {
        List<String> output = new ArrayList<String>();
        StringBuilder resp = new StringBuilder();
        if (this.getDice().size() > 0 || this.getAutoResultReactions().size() > 0) {
            ResultReport report = this.rollDiceWith(0, 0, 0, 0, 0, 0);
            //build the output
            if(isDiscord){
                StringBuilder rolledDice = new StringBuilder();
                // print the dice rolled + any auto results
                for(String r : report.getDiceReactions()) {
                    rolledDice.append(r);
                }
                resp.append("\r\n\r\n");
                if(!discordId.equals("")) {
                    output.add(rolledDice.toString());
                }
                resp.append(String.format(discordIdFormat, discordId));
                resp.append(report.getNetResult().toDiscordString());
            } else {
                resp.append(report.getNetResult().toString());
            }
        }  else {
            resp.append(ERR_NODICE);
        }

        output.add(resp.toString());
        return output;
    }

    /*
        Getters / Setters
     */
    public List<IDie> getDice() {
        return dice;
    }

    public ArrayList<String> getAutoResultReactions() {
        return autoResultReactions;
    }

    public int getAutoS() {
        return autoS;
    }

    public void setAutoS(int autoS) {
        this.autoS = autoS;
    }

    public int getAutoA() {
        return autoA;
    }

    public void setAutoA(int autoA) {
        this.autoA = autoA;
    }

    public int getAutoF() {
        return autoF;
    }

    public void setAutoF(int autoF) {
        this.autoF = autoF;
    }

    public int getAutoT() {
        return autoT;
    }

    public void setAutoT(int autoT) {
        this.autoT = autoT;
    }
}
