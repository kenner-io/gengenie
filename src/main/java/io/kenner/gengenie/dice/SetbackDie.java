package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Reactions;

public class SetbackDie extends Die implements IDie {

    public SetbackDie() {
        sides = 6;
    }

    @Override
    public Result roll() {
        return ResultSets.GetSetbackResult(Die.RollD(sides));
    }

    @Override
    public String getReaction(int rawResult) {
        switch (rawResult) {
            case 0:
                return Reactions.SETBACK_GIF;
            case 1:
            case 2:
                return Reactions.SETBACK_BLANK;
            case 3:
            case 4:
                return Reactions.SETBACK_F;
            case 5:
            case 6:
                return Reactions.SETBACK_T;
            default:
                return "";
        }
    }
}
