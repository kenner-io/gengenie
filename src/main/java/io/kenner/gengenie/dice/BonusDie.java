package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Reactions;

public class BonusDie extends Die implements IDie {

    public BonusDie() {
        sides = 6;
    }

    @Override
    public Result roll() {
        return ResultSets.GetBonusResult(Die.RollD(sides));
    }

    @Override
    public String getReaction(int rawResult) {
        switch (rawResult) {
            case 0:
                return Reactions.BONUS_GIF;
            case 1:
            case 2:
                return Reactions.BONUS_BLANK;
            case 3:
                return Reactions.BONUS_S;
            case 4:
                return Reactions.BONUS_SA;
            case 5:
                return Reactions.BONUS_AA;
            case 6:
                return Reactions.BONUS_A;
            default:
                return "";
        }
    }
}
