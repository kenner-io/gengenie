package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Reactions;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

public class Result {
    protected int successes;
    protected int failures;
    protected int advantage;
    protected int threat;
    protected int triumphs;
    protected int despairs;

    /**
     * raw is the the pre-conversion die result
     */
    protected int raw;

    private static final String discordResultFmt = "%s %d";

    public Result() {
        successes = failures = advantage = threat = triumphs = despairs = raw = 0;
    }

    public Result(int successes, int failures, int advantage, int threat, int triumphs, int despairs, int raw) {
        this.successes = successes;
        this.failures = failures;
        this.advantage = advantage;
        this.threat = threat;
        this.triumphs = triumphs;
        this.despairs = despairs;
        this.raw = raw;
    }

    /**
     * Returns a DieResult that nets success/failures and advantage/threat into consolidated results.
     * @return
     */
    public Result getNetResult() {
        Result result = new Result();

        // Net each result type
        int netSuccess = successes - failures + triumphs - despairs;
        int netAdvantage = advantage - threat;

        // set correct net on result for successes/failures
        if(netSuccess > 0) {
            result.setSuccesses(netSuccess);
        } else {
            result.setFailures(Math.abs(netSuccess));
        }

        // set correct net on result for advantage/threat
        if(netAdvantage > 0) {
            result.setAdvantage(netAdvantage);
        } else {
            result.setThreat(Math.abs(netAdvantage));
        }

        // triumphs and despairs do not cancel each other, return raw
        result.setTriumphs(triumphs);
        result.setDespairs(despairs);

        return result;
    }

    /**
     * Accumulates each Result object in a collection into a single Result
     * @param results
     * @return
     */
    public static Result AccumulateResults(Collection<Result> results) {
        Result accumResult = new Result();
        for(Result r : results) {
            accumResult.successes += r.successes;
            accumResult.failures += r.failures;
            accumResult.advantage += r.advantage;
            accumResult.threat += r.threat;
            accumResult.triumphs += r.triumphs;
            accumResult.despairs += r.despairs;
        }
        return accumResult;
    }

    @Override
    public String toString() {
        boolean firstResult = true;
        StringBuilder sb = new StringBuilder();
        if(successes > 0) {
            sb.append("s: " + successes);
            firstResult = false;
        }

        if(failures > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(" f: " + failures);
        }

        if(advantage > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(" a: " + advantage);
        }

        if(threat > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(" t: " + threat);
        }

        if(triumphs > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(" tri: " + triumphs);
        }

        if(despairs > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(" des: " + despairs);
        }

        if(raw > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(" raw: " + raw);
        }

        if(sb.length() == 0){
            sb.append("No results.");
        }

        return sb.toString();
    }

    /**
     * Returns output best fitted for discord
     * @return
     */
    public String toDiscordString() {
        boolean firstResult = true;
        StringBuilder sb = new StringBuilder();
        if(triumphs > 0) {
            firstResult = false;
            sb.append(String.format(discordResultFmt, Reactions.TRIUMPH, triumphs));
        }

        if(despairs > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(String.format(discordResultFmt, Reactions.DESPAIR, despairs));
        }

        if(successes > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(String.format(discordResultFmt, Reactions.SUCCESS, successes));
        }

        if(failures > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(String.format(discordResultFmt, Reactions.FAILURE, failures));
        }

        if(advantage > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(String.format(discordResultFmt, Reactions.ADVANTAGE, advantage));
        }

        if(threat > 0) {
            if(!firstResult) {
                sb.append(" ");
            } else {
                firstResult = false;
            }
            sb.append(String.format(discordResultFmt, Reactions.THREAT, threat));
        }

        if(sb.length() == 0){
            sb.append("No results.");
        }

        return sb.toString();
    }

    /*
        Getters / Setters
     */

    public int getSuccesses() {
        return successes;
    }

    public void setSuccesses(int successes) {
        this.successes = successes;
    }

    public int getFailures() {
        return failures;
    }

    public void setFailures(int failures) {
        this.failures = failures;
    }

    public int getAdvantage() {
        return advantage;
    }

    public void setAdvantage(int advantage) {
        this.advantage = advantage;
    }

    public int getThreat() {
        return threat;
    }

    public void setThreat(int threat) {
        this.threat = threat;
    }

    public int getTriumphs() {
        return triumphs;
    }

    public void setTriumphs(int triumphs) {
        this.triumphs = triumphs;
    }

    public int getDespairs() {
        return despairs;
    }

    public void setDespairs(int despairs) {
        this.despairs = despairs;
    }

    public int getRaw() {
        return raw;
    }
}
