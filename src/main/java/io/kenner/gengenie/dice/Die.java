package io.kenner.gengenie.dice;

import java.util.Random;

public class Die implements IDie {
    protected int sides;
    private static Random random;

    // static constructor for the random number generator.
    static {
        random = new Random();
    }

    public Die() {
        this.sides = 20;
    }

    public Die(int sides) {
        this.sides = sides;
    }

    @Override
    public Result roll() {
        int raw = RollD(sides);
        return new Result(0, 0, 0, 0, 0, 0, raw);
    }

    @Override
    public String getReaction(int rawResult) {
        return "";
    }

    /**
     * Returns the result of a D(X) roll.  D(20) would return a random number between 1 and 20.
     * @param sides
     * @return
     */
    public static int RollD(int sides){
        return random.nextInt(sides)+1;
    }

    public static Random GetRandom() {
        return random;
    }
}
