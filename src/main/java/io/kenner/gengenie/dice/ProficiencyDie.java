package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Reactions;

public class ProficiencyDie extends Die implements IDie {

    public ProficiencyDie() {
        sides = 12;
    }

    @Override
    public Result roll() {
        return ResultSets.GetProficiencyResult(Die.RollD(sides));
    }

    @Override
    public String getReaction(int rawResult) {
        switch (rawResult) {
            case 0:
                return Reactions.PROFICIENCY_GIF;
            case 1:
                return Reactions.PROFICIENCY_BLANK;
            case 2:
            case 3:
                return Reactions.PROFICIENCY_S;
            case 4:
            case 5:
                return Reactions.PROFICIENCY_SS;
            case 6:
                return Reactions.PROFICIENCY_A;
            case 7:
            case 8:
            case 9:
                return Reactions.PROFICIENCY_SA;
            case 10:
            case 11:
                return Reactions.PROFICIENCY_AA;
            case 12:
                return Reactions.PROFICIENCY_TRI;
            default:
                return "";
        }
    }
}
