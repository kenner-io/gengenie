package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Reactions;

public class ChallengeDie extends Die implements IDie {

    public ChallengeDie() {
        sides = 12;
    }

    @Override
    public Result roll() {
        return ResultSets.GetChallengeResult(Die.RollD(sides));
    }

    @Override
    public String getReaction(int rawResult) {
        switch (rawResult) {
            case 0:
                return Reactions.CHALLENGE_GIF;
            case 1:
                return Reactions.CHALLENGE_BLANK;
            case 2:
            case 3:
                return Reactions.CHALLENGE_F;
            case 4:
            case 5:
                return Reactions.CHALLENGE_FF;
            case 6:
            case 7:
                return Reactions.CHALLENGE_T;
            case 8:
            case 9:
                return Reactions.CHALLENGE_FT;
            case 10:
            case 11:
                return Reactions.CHALLENGE_TT;
            case 12:
                return Reactions.CHALLENGE_DES;
            default:
                return "";
        }
    }
}
