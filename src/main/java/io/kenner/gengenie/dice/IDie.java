package io.kenner.gengenie.dice;

public interface IDie {
    public Result roll();
    public String getReaction(int rawResult);
}
