package io.kenner.gengenie.dice;

import java.util.ArrayList;
import java.util.List;

/**
 * ResultSets holds the static list mappings for each type of die.
 *
 * Result source: Table I.1-2 Core Rulebook
 */
public class ResultSets {
    private static final List<Result> proficiencyResults = new ArrayList<Result>(12);
    private static final List<Result> challengeResults = new ArrayList<Result>(12);
    private static final List<Result> abilityResults = new ArrayList<Result>(8);
    private static final List<Result> difficultyResults = new ArrayList<Result>(8);
    private static final List<Result> bonusResults = new ArrayList<Result>(6);
    private static final List<Result> setbackResults = new ArrayList<Result>(6);
    private static final String rawRollOutOfBounds = "Raw roll (%d) is out of bounds for result set size (%d-%d).";

    // Static constructor; Populate result lists
    static {
        // populate results for proficiency dice
        proficiencyResults.add(0, new Result(0,0,0,0,0,0,1));
        proficiencyResults.add(1, new Result(1,0,0,0,0,0,2));
        proficiencyResults.add(2, new Result(1,0,0,0,0,0,3));
        proficiencyResults.add(3, new Result(2,0,0,0,0,0,4));
        proficiencyResults.add(4, new Result(2,0,0,0,0,0,5));
        proficiencyResults.add(5, new Result(0,0,1,0,0,0,6));
        proficiencyResults.add(6, new Result(1,0,1,0,0,0,7));
        proficiencyResults.add(7, new Result(1,0,1,0,0,0,8));
        proficiencyResults.add(8, new Result(1,0,1,0,0,0,9));
        proficiencyResults.add(9, new Result(0,0,2,0,0,0,10));
        proficiencyResults.add(10, new Result(0,0,2,0,0,0,11));
        proficiencyResults.add(11, new Result(0,0,0,0,1,0,12));

        // populate results for challenge dice
        challengeResults.add(0, new Result(0,0,0,0,0,0,1));
        challengeResults.add(1, new Result(0,1,0,0,0,0,2));
        challengeResults.add(2, new Result(0,1,0,0,0,0,3));
        challengeResults.add(3, new Result(0,2,0,0,0,0,4));
        challengeResults.add(4, new Result(0,2,0,0,0,0,5));
        challengeResults.add(5, new Result(0,0,0,1,0,0,6));
        challengeResults.add(6, new Result(0,0,0,1,0,0,7));
        challengeResults.add(7, new Result(0,1,0,1,0,0,8));
        challengeResults.add(8, new Result(0,1,0,1,0,0,9));
        challengeResults.add(9, new Result(0,0,0,2,0,0,10));
        challengeResults.add(10, new Result(0,0,0,2,0,0,11));
        challengeResults.add(11, new Result(0,0,0,0,0,1,12));

        // populate results for ability dice
        abilityResults.add(0, new Result(0,0,0,0,0,0,1));
        abilityResults.add(1, new Result(1,0,0,0,0,0,2));
        abilityResults.add(2, new Result(1,0,0,0,0,0,3));
        abilityResults.add(3, new Result(2,0,0,0,0,0,4));
        abilityResults.add(4, new Result(0,0,1,0,0,0,5));
        abilityResults.add(5, new Result(0,0,1,0,0,0,6));
        abilityResults.add(6, new Result(1,0,1,0,0,0,7));
        abilityResults.add(7, new Result(0,0,2,0,0,0,8));

        // populate results for difficulty dice
        difficultyResults.add(0, new Result(0,0,0,0,0,0,1));
        difficultyResults.add(1, new Result(0,1,0,0,0,0,2));
        difficultyResults.add(2, new Result(0,2,0,0,0,0,3));
        difficultyResults.add(3, new Result(0,0,0,1,0,0,4));
        difficultyResults.add(4, new Result(0,0,0,1,0,0,5));
        difficultyResults.add(5, new Result(0,0,0,1,0,0,6));
        difficultyResults.add(6, new Result(0,0,0,2,0,0,7));
        difficultyResults.add(7, new Result(0,1,0,1,0,0,8));

        // populate results for bonus dice
        bonusResults.add(0, new Result(0,0,0,0,0,0,1));
        bonusResults.add(1, new Result(0,0,0,0,0,0,2));
        bonusResults.add(2, new Result(1,0,0,0,0,0,3));
        bonusResults.add(3, new Result(1,0,1,0,0,0,4));
        bonusResults.add(4, new Result(0,0,2,0,0,0,5));
        bonusResults.add(5, new Result(0,0,1,0,0,0,6));

        // populate results for setback dice
        setbackResults.add(0, new Result(0,0,0,0,0,0,1));
        setbackResults.add(1, new Result(0,0,0,0,0,0,2));
        setbackResults.add(2, new Result(0,1,0,0,0,0,3));
        setbackResults.add(3, new Result(0,1,0,0,0,0,4));
        setbackResults.add(4, new Result(0,0,0,1,0,0,5));
        setbackResults.add(5, new Result(0,0,0,1,0,0,6));
    }

    /**
     * Returns the appropriate result translated from a D12 roll
     * @param rawRoll
     * @return
     * @throws IndexOutOfBoundsException
     */
    public static Result GetProficiencyResult(int rawRoll) throws IndexOutOfBoundsException {
        if(rawRoll < 1 || rawRoll > proficiencyResults.size()) {
            throw new IndexOutOfBoundsException(String.format(rawRollOutOfBounds, rawRoll, 1, proficiencyResults.size()));
        }

        return proficiencyResults.get(rawRoll-1);
    }

    /**
     * Returns the appropriate result translated from a D12 roll
     * @param rawRoll
     * @return
     * @throws IndexOutOfBoundsException
     */
    public static Result GetChallengeResult(int rawRoll) throws IndexOutOfBoundsException {
        if(rawRoll < 1 || rawRoll > challengeResults.size()) {
            throw new IndexOutOfBoundsException(String.format(rawRollOutOfBounds, rawRoll, 1, challengeResults.size()));
        }

        return challengeResults.get(rawRoll-1);
    }

    /**
     * Returns the appropriate result translated from a D8 roll
     * @param rawRoll
     * @return
     * @throws IndexOutOfBoundsException
     */
    public static Result GetAbilityResult(int rawRoll) throws IndexOutOfBoundsException {
        if(rawRoll < 1 || rawRoll > abilityResults.size()) {
            throw new IndexOutOfBoundsException(String.format(rawRollOutOfBounds, rawRoll, 1, abilityResults.size()));
        }

        return abilityResults.get(rawRoll-1);
    }

    /**
     * Returns the appropriate result translated from a D8 roll
     * @param rawRoll
     * @return
     * @throws IndexOutOfBoundsException
     */
    public static Result GetDifficultyResult(int rawRoll) throws IndexOutOfBoundsException {
        if(rawRoll < 1 || rawRoll > difficultyResults.size()) {
            throw new IndexOutOfBoundsException(String.format(rawRollOutOfBounds, rawRoll, 1, difficultyResults.size()));
        }

        return difficultyResults.get(rawRoll-1);
    }

    /**
     * Returns the appropriate result translated from a D6 roll
     * @param rawRoll
     * @return
     * @throws IndexOutOfBoundsException
     */
    public static Result GetBonusResult(int rawRoll) throws IndexOutOfBoundsException {
        if(rawRoll < 1 || rawRoll > bonusResults.size()) {
            throw new IndexOutOfBoundsException(String.format(rawRollOutOfBounds, rawRoll, 1, bonusResults.size()));
        }

        return bonusResults.get(rawRoll-1);
    }

    /**
     * Returns the appropriate result translated from a D6 roll
     * @param rawRoll
     * @return
     * @throws IndexOutOfBoundsException
     */
    public static Result GetSetbackResult(int rawRoll) throws IndexOutOfBoundsException {
        if(rawRoll < 1 || rawRoll > setbackResults.size()) {
            throw new IndexOutOfBoundsException(String.format(rawRollOutOfBounds, rawRoll, 1, setbackResults.size()));
        }

        return setbackResults.get(rawRoll-1);
    }

    /*
        Getters - no setters.
     */
    public static List<Result> getProficiencyResults() {
        return proficiencyResults;
    }

    public static List<Result> getChallengeResults() {
        return challengeResults;
    }

    public static List<Result> getAbilityResults() {
        return abilityResults;
    }

    public static List<Result> getDifficultyResults() {
        return difficultyResults;
    }

    public static List<Result> getBonusResults() {
        return bonusResults;
    }

    public static List<Result> getSetbackResults() {
        return setbackResults;
    }
}
