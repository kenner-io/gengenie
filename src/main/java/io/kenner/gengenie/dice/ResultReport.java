package io.kenner.gengenie.dice;

import java.util.ArrayList;
import java.util.List;

/**
 * ResultReport is a consolidated collection of objects generated during a dice pool roll.  This includes individual results,
 * an accumulated result, and a net result.
 */
public class ResultReport {
    private List<Result> individualResults;
    private Result accumulatedResult;
    private Result netResult;
    private List<String> diceReactions;

    public ResultReport() {
        individualResults = new ArrayList<Result>();
        diceReactions = new ArrayList<String>();
        accumulatedResult = new Result();
        netResult = new Result();
    }

    public ResultReport(List<Result> individualResults, Result accumulatedResult, Result netResult, List<String> diceReactions) {
        this.individualResults = individualResults;
        this.accumulatedResult = accumulatedResult;
        this.netResult = netResult;
        this.diceReactions = diceReactions;
    }

    /*
        Getters / Setters
     */
    public List<Result> getIndividualResults() {
        return individualResults;
    }

    public void setIndividualResults(List<Result> individualResults) {
        this.individualResults = individualResults;
    }

    public Result getAccumulatedResult() {
        return accumulatedResult;
    }

    public void setAccumulatedResult(Result accumulatedResult) {
        this.accumulatedResult = accumulatedResult;
    }

    public Result getNetResult() {
        return netResult;
    }

    public void setNetResult(Result netResult) {
        this.netResult = netResult;
    }

    public List<String> getDiceReactions() {
        return diceReactions;
    }

    public void setDiceReactions(List<String> diceReactions) {
        this.diceReactions = diceReactions;
    }
}
