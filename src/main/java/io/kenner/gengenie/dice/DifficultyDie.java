package io.kenner.gengenie.dice;

import io.kenner.gengenie.input.Reactions;

public class DifficultyDie extends Die implements IDie {

    public DifficultyDie() {
        sides = 8;
    }

    @Override
    public Result roll() {
        return ResultSets.GetDifficultyResult(Die.RollD(sides));
    }

    @Override
    public String getReaction(int rawResult) {
        switch (rawResult) {
            case 0:
                return Reactions.DIFFICULTY_GIF;
            case 1:
                return Reactions.DIFFICULTY_BLANK;
            case 2:
                return Reactions.DIFFICULTY_F;
            case 3:
                return Reactions.DIFFICULTY_FF;
            case 4:
            case 5:
            case 6:
                return Reactions.DIFFICULTY_T;
            case 7:
                return Reactions.DIFFICULTY_TT;
            case 8:
                return Reactions.DIFFICULTY_FT;
            default:
                return "";
        }
    }
}
