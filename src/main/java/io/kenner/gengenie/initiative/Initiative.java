package io.kenner.gengenie.initiative;

import io.kenner.gengenie.input.SetDiceCommand;
import io.kenner.gengenie.redis.Redis;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "initiative")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Initiative implements Serializable {
    private static final long serialVersionUID = 1L;
    // id is the discord channel id
    private String id;
    private List<String> playerIds;
    private List<InitCharacter> npcs;

    @XmlTransient
    private List<InitCharacter> players;
    @XmlTransient
    private static final String keyFmt = "initiative.%s";

    // required by jaxb
    public Initiative() {
        this.playerIds = new ArrayList<String>();
        this.npcs = new ArrayList<InitCharacter>();
    }

    public Initiative(String id) {
        this.id = id;
        this.playerIds = new ArrayList<String>();
        this.npcs = new ArrayList<InitCharacter>();
    }

    public Initiative(String id, List<String> playerIds, List<InitCharacter> npcs) {
        this.id = id;
        this.playerIds = playerIds;
        this.npcs = npcs;
    }

    /**
     * Attempts to read from Redis.  If record doesn't exist, returns basic object.
     * @param id
     * @return
     */
    public static Initiative getOrCreate(String id) {
        Initiative init = new Initiative(id);
        String redisKey = String.format(keyFmt, id);
        String redisValue = "";
        try {
            redisValue = Redis.get(redisKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(redisValue != null) {
            Initiative storedResult = Initiative.fromJsonString(redisValue);
            if(storedResult != null) {
                try {
                    storedResult.loadPlayers();
                } catch (Exception e) {
                    // don't fail, just log the error
                    e.printStackTrace();
                }
                if(storedResult.getNpcs() != null) {
                    for(InitCharacter npc : storedResult.getNpcs()) {
                        npc.loadCrits();
                    }
                }
                if(storedResult.getPlayers() != null) {
                    for(InitCharacter pc : storedResult.getPlayers()) {
                        pc.loadCrits();
                    }
                }
                return storedResult;
            }
        }
        // unable to get value from Redis, return base object.
        return init;
    }

    /**
     * Reads player records using id list stored in Redis
     * @throws Exception
     */
    public void loadPlayers() throws Exception{
        if(this.getPlayerIds() == null) {
            return;
        }
        this.players = new ArrayList<InitCharacter>();
        for(String id : this.getPlayerIds()) {
            // load player cool / vigiliance from redis
            String cool = Redis.get(String.format(SetDiceCommand.keyFormat, id, "cool"));
            String vigilance = Redis.get(String.format(SetDiceCommand.keyFormat, id, "vigilance"));
            InitCharacter pc = new InitCharacter(id, true, cool, vigilance);
            this.players.add(pc);
        }
    }

    /**
     * Attempts to write to Redis.  Returns a string error if one occurs.
     * @return
     */
    public String write() {
        try {
            Redis.set(String.format(keyFmt, this.id), this.toJsonString());
        } catch (Exception e) {
            e.printStackTrace();
            return "Unable to update initiative order.";
        }
        return "";
    }

    /**
     * Attempts to unmarshall from a json string.  Returns null on failure
     * @param json
     * @return
     */
    public static Initiative fromJsonString(String json) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(Initiative.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
            jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

            Initiative init = (Initiative)jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
            return init;
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Unmarshalling failed
        return null;
    }

    /**
     * Attempts to marshall into a json string.  Returns empty string on failure
     * @return
     */
    public String toJsonString() throws Exception{
        String json = "";
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Initiative.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

            //Print JSON String to Console
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(this, sw);

            json = sw.toString();
        } catch (Exception e) {
            //e.printStackTrace();
            throw e;
        }

        return json;
    }

    /* Getters / Setters */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getPlayerIds() {
        return playerIds;
    }

    public void setPlayerIds(List<String> playerIds) {
        this.playerIds = playerIds;
    }

    public List<InitCharacter> getNpcs() {
        return npcs;
    }

    public void setNpcs(List<InitCharacter> npcs) {
        this.npcs = npcs;
    }

    public List<InitCharacter> getPlayers() {
        return players;
    }

    public void setPlayers(List<InitCharacter> players) {
        this.players = players;
    }
}


