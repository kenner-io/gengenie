package io.kenner.gengenie.initiative;

import io.kenner.gengenie.dice.SkillDice;
import io.kenner.gengenie.input.SetDiceCommand;
import io.kenner.gengenie.redis.Redis;

import java.util.ArrayList;
import java.util.Collections;

/**
 * InitiativeManager is a static management system for the bot.
 *
 * Each channel can have its own initiative order
 */
public class InitiativeManager {
    private static final String alreadyInOrderErr = "<@%s> is already in the turn order.";
    private static final String npcAlreadyInOrderErr = "%s is already in the turn order.";
    private static final String addedToOrderErr = "<@%s> added to the turn order.";
    private static final String npcAddedToOrderErr = "%s added to the turn order.";
    private static final String missingStatsErr = "<@%s> you must set your %s dice pools using `setdice` before being added to initiative.";
    private static final String npcMissingStatsErr = "%s must have both cool and vigilance defined before being added to initiative.";

    public static String addToInitiative(String id, boolean isPlayer, String charId, String cool, String vigilance) {
        // Get the existing record, or a clean one
        Initiative init = Initiative.getOrCreate(id);

        if(isPlayer) {
            // make sure the player doesn't exist on the current record.
            for(String pid : init.getPlayerIds()) {
                if(pid.equals(charId)) {
                    return String.format(alreadyInOrderErr, charId);
                }
            }

            // we load the player dicepools for cool / vigilance
            // if they are not set, respond that they must set their pools first.
            cool = "";
            vigilance = "";
            try {
                cool = Redis.get(String.format(SetDiceCommand.keyFormat, charId, "cool"));
                vigilance = Redis.get(String.format(SetDiceCommand.keyFormat, charId, "vigilance"));
                if (cool == null && vigilance == null) {
                    return String.format(missingStatsErr, charId, "cool and vigilance");
                } else if (cool == null) {
                    return String.format(missingStatsErr, charId, "cool");
                } else if (vigilance == null) {
                    return String.format(missingStatsErr, charId, "vigilance");
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Error reading player dice pools.";
            }

            //if they don't, add them.
            init.getPlayerIds().add(charId);

            // update the order in redis
            String writeErr = init.write();
            if(!writeErr.equals("")) {
                return writeErr;
            } else {
                return String.format(addedToOrderErr, charId);
            }
        } else {
            // make sure the npc doesn't exist on the current record.
            for(InitCharacter npc : init.getNpcs()) {
                if(npc.getId().equals(charId)) {
                    return String.format(npcAlreadyInOrderErr, charId);
                }
            }

            // make sure both cool and vigilance are non-empty
            if(cool.isEmpty() || vigilance.isEmpty()) {
                return String.format(npcMissingStatsErr, charId);
            }

            // create the npc record
            InitCharacter npc = new InitCharacter(charId, false, cool, vigilance);

            //if they don't, add them.
            init.getNpcs().add(npc);

            // update the order in redis
            String writeErr = init.write();
            if(!writeErr.equals("")) {
                return writeErr;
            } else {
                return String.format(npcAddedToOrderErr, charId);
            }
        }
    }

    /**
     * returns an unrolled initiative report.
     * @param channelId
     * @return
     */
    public static String getInitReportDiscordString(String channelId) {
        Initiative report = Initiative.getOrCreate(channelId);

        // build output
        StringBuilder sb = new StringBuilder();
        sb.append("Initiative detail for <#"+channelId+">.\r\n");
        if((report.getPlayers() != null && report.getPlayers().size() > 0) || (report.getNpcs() != null && report.getNpcs().size() > 0)) {
            sb.append(String.format("%-10s | %-20s | %-20s\r\n", "Character", "Cool", "Vigilance"));
        } else {
            sb.append("No characters in initiative.");
            return sb.toString();
        }

        if (report.getPlayers() == null || report.getPlayers().size() == 0) {
            sb.append("No players in turn order.\r\n");
        } else {
            for (InitCharacter pc : report.getPlayers()) {
                SkillDice cool = SkillDice.parse(pc.getCool());
                SkillDice vigilance = SkillDice.parse(pc.getVigilance());
                sb.append(String.format("%-10s | %-20s | %-20s\r\n", String.format("<@%s>",pc.getId()), cool.getBlankDice(), vigilance.getBlankDice()));
            }
        }

        if (report.getNpcs() == null || report.getNpcs().size() == 0) {
            sb.append("No npcs in turn order.\r\n");
        } else {
            for (InitCharacter npc : report.getNpcs()) {
                SkillDice cool = SkillDice.parse(npc.getCool());
                SkillDice vigilance = SkillDice.parse(npc.getVigilance());
                sb.append(String.format("%-10s | %-20s | %-20s\r\n", npc.getId(), cool.getBlankDice(), vigilance.getBlankDice()));
            }
        }

        return sb.toString();
    }

    public static String clearInit(String channelId, boolean clearPlayers, boolean clearNpcs) {
        Initiative init = Initiative.getOrCreate(channelId);
        if(clearPlayers) {
            init.setPlayerIds(new ArrayList<String>());
        }
        if(clearNpcs) {
            init.setNpcs(new ArrayList<InitCharacter>());
        }
        String writeErr = init.write();
        if(!writeErr.isEmpty()){
            return writeErr;
        } else {
            return "Initiative updated.";
        }
    }

    public static String roll(String channelId, String playerPool, String npcPool) {
        // validate pool input
        if(!playerPool.equalsIgnoreCase("cool") && !playerPool.equalsIgnoreCase("vigilance")) {
            return "Invalid player skill pool.  Valid choices are `cool` or `vigilance`.";
        }
        if(!npcPool.equalsIgnoreCase("cool") && !npcPool.equalsIgnoreCase("vigilance")) {
            return "Invalid npc skill pool.  Valid choices are `cool` or `vigilance`.";
        }

        // get the initiative table
        Initiative init = Initiative.getOrCreate(channelId);
        if(init.getNpcs().isEmpty() && init.getPlayers().isEmpty()) {
            return "Must add characters to the initiative first!";
        }

        // Get the player roll results
        ArrayList<InitResultReport> playerResults = new ArrayList<InitResultReport>();
        for(InitCharacter pc : init.getPlayers()) {
            InitResultReport r = new InitResultReport();
            r.setCharacter(pc);
            switch(playerPool){
                case "cool":
                    r.setInitResult(pc.rollCool());
                    break;
                case "vigilance":
                    r.setInitResult(pc.rollVigilance());
                    break;
            }
            playerResults.add(r);
        }
        // Sort the results
        Collections.sort(playerResults, Collections.reverseOrder());

        // Get the npc roll results
        ArrayList<InitResultReport> npcResults = new ArrayList<InitResultReport>();
        for(InitCharacter npc : init.getNpcs()) {
            InitResultReport r = new InitResultReport();
            r.setCharacter(npc);
            switch(playerPool){
                case "cool":
                    r.setInitResult(npc.rollCool());
                    break;
                case "vigilance":
                    r.setInitResult(npc.rollVigilance());
                    break;
            }
            npcResults.add(r);
        }
        // Sort the results
        Collections.sort(npcResults, Collections.reverseOrder());

        // Now that we have rolled and sorted player/npc inits, lets
        // consolidate them into the actual turn order.
        boolean pcTurn = true;
        if(playerResults.isEmpty()) {
            pcTurn = false;
        } else {
            if(!npcResults.isEmpty()){
                int comp = playerResults.get(0).compareTo(npcResults.get(0));
                // (-)LT, (0)==, (+)GT
                if(comp < 0) {
                    // ties go to the player character.
                    pcTurn = false;
                }
            }
        }

        ArrayList<InitResultReport> turnOrder = new ArrayList<InitResultReport>();
        int pIndex = 0;
        int nIndex = 0;
        for(int i = 0; i < playerResults.size() + npcResults.size(); i++) {
            if(pcTurn && pIndex < playerResults.size()) {
                turnOrder.add(playerResults.get(pIndex));
                pIndex++;
                pcTurn = false;
            } else {
                turnOrder.add(npcResults.get(nIndex));
                nIndex++;
                pcTurn = true;
            }
        }

        // we now have a rolled, sorted, consolidated turn order.  Compile result list.
        StringBuilder sb = new StringBuilder();
        sb.append("Initiative detail for <#"+channelId+">.\r\n\r\n");

        // report free turns
        boolean hasFreeTurns = false;
        for(InitResultReport turn : turnOrder ) {
            if(turn.getInitResult().getNetResult().getTriumphs() > 0) {
                if(!hasFreeTurns) {
                    hasFreeTurns = true;
                    sb.append("Free actions:\r\n");
                }
                if(turn.getCharacter().isPlayer()){
                    sb.append(String.format("%s has %d free action(s).\r\n", String.format("<@%s>", turn.getCharacter().getId()), turn.getInitResult().getNetResult().getTriumphs()));
                } else {
                    sb.append(String.format("%s has %d free action(s).\r\n", turn.getCharacter().getId(), turn.getInitResult().getNetResult().getTriumphs()));
                }
            }
        }
        if(hasFreeTurns) {
            sb.append("\r\n");
        }

        // print turn order
        for(InitResultReport turn : turnOrder ) {
            if(turn.getCharacter().isPlayer()){
                sb.append(String.format("%s rolled %s with %s\r\n", String.format("<@%s>", turn.getCharacter().getId()), turn.getInitResult().getDiceReactions(), turn.getInitResult().getNetResult().toDiscordString()));
            } else {
                sb.append(String.format("%s rolled %s with %s.\r\n", turn.getCharacter().getId(), turn.getInitResult().getDiceReactions(), turn.getInitResult().getNetResult().toDiscordString()));
            }
        }

        return sb.toString();
    }
}
