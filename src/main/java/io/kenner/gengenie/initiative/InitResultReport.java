package io.kenner.gengenie.initiative;

import io.kenner.gengenie.dice.ResultReport;

public class InitResultReport implements Comparable<InitResultReport> {
    private InitCharacter character;
    private ResultReport initResult;

    /**
     * Used for sorting initiative order arrays.
     * @param resultReport
     * @return
     */
    @Override
    public int compareTo(InitResultReport resultReport) {
        // Number of successes dictate order, with advantage being a tiebreaker.
        int successWeight = 100000;
        int advWeight = 1;

        int thisScore = (this.initResult.getNetResult().getSuccesses()*successWeight) + (this.initResult.getNetResult().getAdvantage()*advWeight);
        int thatScore = (resultReport.initResult.getNetResult().getSuccesses()*successWeight) + (resultReport.initResult.getNetResult().getAdvantage()*advWeight);

        return thisScore - thatScore;
    }

    /* Getters / Setters */

    public InitCharacter getCharacter() {
        return character;
    }

    public void setCharacter(InitCharacter character) {
        this.character = character;
    }

    public ResultReport getInitResult() {
        return initResult;
    }

    public void setInitResult(ResultReport initResult) {
        this.initResult = initResult;
    }
}
