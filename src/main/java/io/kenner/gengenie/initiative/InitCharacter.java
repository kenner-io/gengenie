package io.kenner.gengenie.initiative;

import io.kenner.gengenie.crit.Crit;
import io.kenner.gengenie.crit.PlayerCrits;
import io.kenner.gengenie.dice.ResultReport;
import io.kenner.gengenie.dice.SkillDice;
import io.kenner.gengenie.redis.Redis;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

// TODO refactor class into its own package, rename to Character
@XmlRootElement(name = "initCharacter")
@XmlAccessorType(XmlAccessType.FIELD)
public class InitCharacter implements Serializable {
    //static properties aren't written to redis
    private static final long serialVersionUID = 1L;
    protected static final String npcKeyFmt = "initiative.npc.%s";
    private static final String playerCritKeyFmt = "player.crits.%s";

    // The character id is the discord user id
    private String id;
    // we load player pools from their dice pools.
    private boolean isPlayer;
    // cool, vigilance, and crits need to be stored/loaded for npcs
    private String cool;
    private String vigilance;
    private List<Integer> critRolls;

    // Transient properties that aren't written to redis
    @XmlTransient
    private SkillDice coolBag;
    @XmlTransient
    private SkillDice vigilBag;
    @XmlTransient
    private ResultReport coolResult;
    @XmlTransient
    private ResultReport vigilResult;
    @XmlTransient
    private List<Crit> crits;

    //required by jaxb
    public InitCharacter() {
        id = "";
        isPlayer = false;
        cool = "";
        vigilance = "";
        this.critRolls = new ArrayList<Integer>();
        this.crits = new ArrayList<Crit>();
    }

    public InitCharacter(String id, boolean isPlayer, String cool, String vigilance) {
        this.id = id;
        this.isPlayer = isPlayer;
        this.cool = cool;
        this.vigilance = vigilance;
        this.critRolls = new ArrayList<Integer>();
        this.crits = new ArrayList<Crit>();
    }

    protected ResultReport rollCool() {
        // Make sure the cool dice are set and parsed
        if(this.coolBag == null) {
            if(this.cool.isEmpty()) {
                this.coolResult = new ResultReport();
                return this.coolResult;
            } else {
                this.coolBag = SkillDice.parse(this.cool);
            }
        }
        // roll them and save the result
        this.coolResult = this.coolBag.roll();
        return this.coolResult;
    }

    protected ResultReport rollVigilance() {
        // Make sure the vigilance dice are set and parsed
        if(this.vigilBag == null) {
            if(this.vigilance.isEmpty()) {
                this.vigilResult = new ResultReport();
                return this.vigilResult;
            } else {
                this.vigilBag = SkillDice.parse(this.vigilance);
            }
        }
        // roll them and save the result
        this.vigilResult = this.vigilBag.roll();
        return this.vigilResult;
    }

    /**
     * We only store the rolls in Redis to conserve space.  We need to saturate Crit objects from cached map.
     */
    public void loadCrits() {
        if (this.isPlayer) {
            PlayerCrits crits = PlayerCrits.getOrCreate(this.id);
            this.crits = crits.getCrits();
            this.critRolls = crits.getCritRolls();
        } else {
            if (this.critRolls == null || this.critRolls.isEmpty()) {
                return;
            }

            for (int rawRoll : this.critRolls) {
                Crit crit = new Crit(rawRoll);
                this.crits.add(crit);
            }
        }
    }

    /**
     * Attempts to write to Redis.  Returns a string error if one occurs.
     * @return
     */
    protected String write() {
        try {
            Redis.set(String.format(npcKeyFmt, this.id), this.toJsonString());
        } catch (Exception e) {
            e.printStackTrace();
            return "Unable to update initiative npc.";
        }
        return "";
    }

    /**
     * Attempts to player crit write to Redis.  Returns a string error if one occurs.
     * @return
     */
    protected String writePlayerCrit() {
        if (!this.isPlayer()) {
            return "Couldn't write player crit, this is an NPC!";
        }
        try {
            Redis.set(String.format(playerCritKeyFmt, this.id), this.toJsonString());
        } catch (Exception e) {
            e.printStackTrace();
            return "Unable to update player tracked crits.";
        }
        return "";
    }

    /**
     * Attempts to unmarshall from a json string.  Returns null on failure
     * @param json
     * @return
     */
    public static InitCharacter fromJsonString(String json) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(InitCharacter.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
            jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

            InitCharacter init = (InitCharacter)jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
            return init;
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Unmarshalling failed
        return null;
    }

    /**
     * Attempts to marshall into a json string.  Returns empty string on failure
     * @return
     */
    public String toJsonString() throws Exception {
        String json = "";
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(InitCharacter.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

            //Print JSON String to Console
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(this, sw);

            json = sw.toString();
        } catch (Exception e) {
            throw e;
        }

        return json;
    }

    /* getters / setters */

    public String getId() {
        return id;
    }

    public boolean isPlayer() {
        return isPlayer;
    }

    public String getCool() {
        return cool;
    }

    public String getVigilance() {
        return vigilance;
    }

    public List<Crit> getCrits() {
        return crits;
    }

    public void setCrits(List<Crit> crits) {
        this.crits = crits;
    }

    public List<Integer> getCritRolls() {
        return critRolls;
    }

    public void setCritRolls(List<Integer> critRolls) {
        this.critRolls = critRolls;
    }
}
