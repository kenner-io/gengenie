package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The help command by itself lists available commands and their usage.  Given an argument (command name), it will
 * attempt to retrieve the help text for that command.
 */
public class HelpCommand implements ICommand {
    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> result = new ArrayList<String>();
        if (args == null || args.length == 0) {
            result.add(this.getHelpText());
            return result;
        }
        String commandName = args[0].toLowerCase();
        if (Parser.commandRegistry.containsKey(commandName)) {
           result.add(Parser.commandRegistry.get(commandName).getHelpText());
        } else {
            // command not found, return parser help error
            result.add(Parser.ERR_HELP);
        }
        return result;
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("Welcome to GenGenie!  The following commands are at your disposal:\r\n");
        sb.append("```");
        for(Map.Entry<String, ICommand> entry : Parser.commandRegistry.entrySet()) {
           sb.append(String.format("%-10s : %s\r\n", entry.getKey(), entry.getValue().getUsageText()));
        }
        sb.append("```");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "help commandName";
    }
}
