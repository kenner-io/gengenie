package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.User;
import io.kenner.gengenie.dice.SkillDice;
import io.kenner.gengenie.redis.Redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GetDiceCommand implements ICommand {
    private static final String numericRegex = "-?\\d+";
    public static final String ERR_NOARGS = "Invalid usage.  Usage: %s";
    public static final String ERR_NODISCORD = "Unable to process command; unable to access discord member.";
    public static final String ERR_NOREDIS = "Unable to process command; no connection to Redis available.";
    public static final String ERR_INVALIDKEY = "Unable to use key (%s).";
    public static final String keyFormat = "%s.dicepool.%s";
    public static final String successfulOutputFmt = "Dice pool (%s) assigned dice (%s).";

    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> output = new ArrayList<String>();
        if (args == null || args.length == 0) {
            output.add(String.format(ERR_NOARGS, getUsageText()));
            return output;
        } else if (!isDiscord || discordEvent == null || discordEvent.getMember() == null) {
            output.add(ERR_NODISCORD);
            return output;
        } else if (!Redis.getConnectionStatus()) {
            output.add(ERR_NOREDIS);
            return output;
        }

        // get the discord member object
        Optional<User> om = discordEvent.getMessage().getAuthor();
        User member = om.get();
        if(member == null) {
            output.add(ERR_NODISCORD);
            return output;
        }

        // get the poolname
        String poolName = args[0].trim().toLowerCase();
        if (poolName.length() == 0) {
            output.add(String.format(ERR_INVALIDKEY, poolName));
            return output;
        }

        // create key for dice pool
        String key = String.format(keyFormat, member.getId().asString(), poolName);
        String value = "";
        try {
            value = Redis.get(key);
        } catch (Exception e) {
            output.add("Unable to retrieve value: " + e.getMessage());
            e.printStackTrace();
            return output;
        }

        String diceOutput = value;
        if (isDiscord && !value.matches(numericRegex)) {
            SkillDice pool = SkillDice.parse(value);
            diceOutput = pool.getBlankDice();
        }
        output.add(String.format(successfulOutputFmt, poolName, diceOutput));
        return output;
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("```");
        sb.append("getdice allows you to retrieve a standard set of dice by poolname.\r\n");
        sb.append("Usage: ");
        sb.append(getUsageText()+"\r\n");
        sb.append("Example: getdice brawl\r\n");
        sb.append("Dice are user specific, if two users set a key of `ranged` they will not conflict.");
        sb.append("```");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "getdice poolname";
    }
}
