package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import io.kenner.gengenie.initiative.InitiativeManager;
import io.kenner.gengenie.redis.Redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InitiativeCommand implements ICommand {
    public static final String ERR_INVALID = "Invalid usage.  Usage: %s";
    public static final String ERR_NODISCORD = "Unable to process command; unable to access discord member.";
    public static final String ERR_NOREDIS = "Unable to process command; no connection to Redis available.";

    // subcommands
    private static final String cmdAddMe = "addme";
    private static final String cmdAddNpc = "addnpc";
    private static final String cmdClearAll = "clearall";
    private static final String cmdClearPlayer = "clearplayers";
    private static final String cmdClearNpc = "clearnpcs";
    private static final String cmdReport = "report";
    private static final String cmdRoll = "roll";



    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> output = new ArrayList<String>();
        if (args == null || args.length == 0) {
            output.add(getHelpText());
            return output;
        } else if (!isDiscord || discordEvent == null || discordEvent.getMember() == null) {
            output.add(ERR_NODISCORD);
            return output;
        } else if (!Redis.getConnectionStatus()) {
            output.add(ERR_NOREDIS);
            return output;
        }

        // get the discord member object
        Optional<User> om = discordEvent.getMessage().getAuthor();
        String channelId = discordEvent.getMessage().getChannelId().asString();
        User member = om.get();
        if(member == null || channelId == null || channelId.equals("")) {
            output.add(ERR_NODISCORD);
            return output;
        }

        // get the commandName
        String cmdName = args[0].trim().toLowerCase();
        if (cmdName.length() == 0) {
            output.add(String.format(ERR_INVALID, getHelpText()));
            return output;
        }

        switch (cmdName) {
            case cmdAddMe:
                output.add(addPlayer(channelId, member.getId().asString()));
                break;
            case cmdAddNpc:
                // validate that cool / vigilance args exist
                if(args.length != 4) {
                    output.add(String.format(ERR_INVALID, getHelpText()));
                    return output;
                }
                output.add(addNpc(channelId, args[1], args[2], args[3]));
                break;
            case cmdReport:
                output.add(getReport(channelId));
                break;
            case cmdClearAll:
                output.add(clearInit(channelId, true, true));
                break;
            case cmdClearPlayer:
                output.add(clearInit(channelId, true, false));
                break;
            case cmdClearNpc:
                output.add(clearInit(channelId, false, true));
                break;
            case cmdRoll:
                // validate that cool / vigilance args exist
                if(args.length != 3) {
                    output.add(String.format(ERR_INVALID, getHelpText()));
                    return output;
                }
                output.add(roll(channelId, args[1], args[2]));
                break;
            default:
                output.add(String.format(ERR_INVALID, getHelpText()));
                return output;
        }

        return output;
    }

    public static String addPlayer(String channelId, String playerId) {
        return InitiativeManager.addToInitiative(channelId, true, playerId, "", "");
    }

    public static String addNpc(String channelId, String npcId, String cool, String vigilance) {
        return InitiativeManager.addToInitiative(channelId, false, npcId, cool, vigilance);
    }

    public static String getReport(String channelId) {
        return InitiativeManager.getInitReportDiscordString(channelId);
    }

    public static String clearInit(String channelId, boolean clearPlayers, boolean clearNpcs) {
        return InitiativeManager.clearInit(channelId, clearPlayers, clearNpcs);
    }

    public static String roll(String channelId, String playerPool, String npcPool) {
        return InitiativeManager.roll(channelId, playerPool.trim().toLowerCase(), npcPool.trim().toLowerCase());
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("```");
        sb.append("initiative has several subfunctions for managing turn order.\r\n");
        sb.append("Usage: ");
        sb.append("`"+getUsageText()+"` will get help text.\r\n");
        sb.append("`init addme` will add you as a PC to the turn order.\r\n");
        sb.append("`init addnpc npcId coolpool vigilancePool` will add an NPC to the turn order.\r\n");
        sb.append("Example: `init addnpc GoldDragon yyyg ygg`.\r\n");
        sb.append("`init addnpc clearall` will clear the players and npcs from the turn order.\r\n");
        sb.append("`init addnpc clearplayers` will clear the players from the turn order.\r\n");
        sb.append("`init addnpc clearnpcs` will clear the npcs from the turn order.\r\n");
        sb.append("`init report` will return a report on the turn order.\r\n");
        sb.append("`init roll playerPool npcPool` will roll the configured initiative and print the turn order.\r\n");
        sb.append("Example: `init roll cool vigilance` will roll the players' cool and the NPCs' vigilance.\r\n");
        sb.append("```");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "init";
    }
}
