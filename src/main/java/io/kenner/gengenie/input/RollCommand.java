package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.User;
import io.kenner.gengenie.dice.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RollCommand implements ICommand {
    public static final String ERR_NODICE = "No dice to roll!";
    private static final String numericRegex = "-?\\d+";
    private static final String dRollFormat = "D%d result: %d";
    private static final String gDieFormat = "%-2s : %s\r\n";
    private static final String autoFormat = "%-2s : %s\r\n";

    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> output = new ArrayList<String>();
        if (args == null || args.length == 0) {
            output.add(ERR_NODICE);
            return output;
        }

        boolean isNumericRoll = false;

        // consolidate args into a single string to be parsed
        StringBuffer sb = new StringBuffer();
        for(String a : args) {
            sb.append(a.trim());
        }
        String arg = sb.toString().toLowerCase();

        // is the arg numeric?  roll a D(X)
        if (arg.matches(numericRegex)) {
            isNumericRoll = true;
            try {
                int sides = Math.abs(Integer.parseInt(arg));
                int rawRoll = Die.RollD(sides);
                output.add(String.format(dRollFormat, sides, rawRoll));
            } catch (Exception e) {
                output.add("Error parsing sides");
            }
            return output;
        }

        // attempt to get discordId, if appropriate.
        String discordId = "";
        if(isDiscord){
            // get the discord member object
            Optional<User> om = discordEvent.getMessage().getAuthor();
            User member = om.get();
            if(member != null) {
                discordId = member.getId().asString();
            }
        }

        // otherwise parse known dice tokens and return a net result
        SkillDice diceBag = SkillDice.parse(arg);
        return diceBag.rollWithOutput(isDiscord, discordId);
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("Roll can take several types of input:\r\n");
        sb.append("To roll a sided die, like a D20, use `roll 20`.\r\n");
        sb.append("To roll a Genesys die, add any number of the following:\r\n");
        sb.append("```");
        sb.append(String.format(gDieFormat, Parser.PROFICIENCY_DIE, "Proficiency (Yellow)"));
        sb.append(String.format(gDieFormat, Parser.ABILITY_DIE, "Ability (Green)"));
        sb.append(String.format(gDieFormat, Parser.BONUS_DIE, "Bonus (Blue)"));
        sb.append(String.format(gDieFormat, Parser.CHALLENGE_DIE, "Challenge (Red)"));
        sb.append(String.format(gDieFormat, Parser.DIFFICULTY_DIE, "Difficulty (Purple)"));
        sb.append(String.format(gDieFormat, Parser.SETBACK_DIE, "Setback (Black)"));
        sb.append("```");
        sb.append("To add automatic results, add any of the following:\r\n");
        sb.append("```");
        sb.append(String.format(autoFormat, Parser.AUTO_SUCCESS, "Success"));
        sb.append(String.format(autoFormat, Parser.AUTO_ADVANTAGE, "Advantage"));
        sb.append(String.format(autoFormat, Parser.AUTO_FAILURE, "Failure"));
        sb.append(String.format(autoFormat, Parser.AUTO_THREAT, "Threat"));
        sb.append("```");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "roll [dicePool or uinteger]";
    }
}
