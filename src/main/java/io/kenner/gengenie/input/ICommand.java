package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;

import java.util.List;

/**
 * Command Interface.  All functions must be implemented to be processable by the Parser.
 */
public interface ICommand {

    /**
     * Processes the command using provided arguments and returns the output text to be displayed to the user.
     * @param args
     * @param discordEvent
     * @return
     */
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent);

    /**
     * Returns output for the "help" command to return to the user.
     * @return
     */
    public String getHelpText();

    /**
     * Returns the suggested usage string.  Example:
     * .gg command [arguments...]
     * @return
     */
    public String getUsageText();
}
