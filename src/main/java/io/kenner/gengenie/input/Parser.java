package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;

import java.util.*;

/**
 * Parser takes text input, maps it to functionality, and returns appropriate text output.
 *
 * Standard format of input shall be:
 * [parseToken] [command] [parameters...]
 * .gg help roll
 *
 * Input Commands
 * help
 * roll
 * dump
 * invite
 * getdice
 * setdice
 * usedice
 * init
 * crit
 */
public class Parser {
    // Default value of .gg for GenGenie.  Not making final; may be worth making configurable in the future.
    public static String PARSE_TOKEN = ".gg";

    // command names
    public static final String CMD_HELP = "help";
    public static final String CMD_ROLL = "roll";
    public static final String CMD_DUMP = "dump";
    public static final String CMD_INVITE = "invite";
    public static final String CMD_GETDICE = "getdice";
    public static final String CMD_SETDICE = "setdice";
    public static final String CMD_USEDICE = "usedice";
    public static final String CMD_INIT = "init";
    public static final String CMD_CRIT = "crit";

    // die symbols
    public static final char PROFICIENCY_DIE = 'y';
    public static final char CHALLENGE_DIE = 'r';
    public static final char ABILITY_DIE = 'g';
    public static final char DIFFICULTY_DIE = 'p';
    public static final char BONUS_DIE = 'b';
    public static final char SETBACK_DIE = 'k';
    public static final char AUTO_SUCCESS = 's';
    public static final char AUTO_ADVANTAGE = 'a';
    public static final char AUTO_FAILURE = 'f';
    public static final char AUTO_THREAT = 't';

    // error text
    public static final String ERR_HELP = "Couldn't process input.  Try `.gg help` to get started.";

    // command registry
    protected static final Map<String, ICommand> commandRegistry = new HashMap<String, ICommand>();
    static {
        // register function classes
        commandRegistry.put(CMD_HELP, new HelpCommand());
        commandRegistry.put(CMD_ROLL, new RollCommand());
        commandRegistry.put(CMD_DUMP, new DumpCommand());
        commandRegistry.put(CMD_INVITE, new InviteCommand());
        commandRegistry.put(CMD_SETDICE, new SetDiceCommand());
        commandRegistry.put(CMD_GETDICE, new GetDiceCommand());
        commandRegistry.put(CMD_USEDICE, new UseDiceCommand());
        commandRegistry.put(CMD_INIT, new InitiativeCommand());
        commandRegistry.put(CMD_CRIT, new CritCommand());
    }

    /**
     * Checks input to ensure text begins with PARSE_TOKEN, returns an empty string if it doesn't.
     * Otherwise, checks the command registry and hands off, or returns help command text if unrecognized.
     *
     * @param input
     * @param discordEvent
     * @return
     */
    public static List<String> Process(String input, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> response = new ArrayList<String>();
        String[] tokens = input.split(" ");
        if (tokens.length == 0 || !tokens[0].equalsIgnoreCase(PARSE_TOKEN)) {
            // Don't process
            return response;
        } else if (tokens.length < 2) {
            // couldn't have specified a command name, but starts with PARSE_TOKEN.  Return help suggestion.
            response.add(ERR_HELP);
            return response;
        }

        // assign remaining input tokens
        String command = tokens[1].toLowerCase();
        String[] args = null;
        if (tokens.length > 2) {
            args = Arrays.copyOfRange(tokens, 2, tokens.length);
        }

        // attempt to parse command
        if (!commandRegistry.containsKey(command)) {
            // command isn't registered, display help prompt.
            response.add(ERR_HELP);
            return response;
        }
        ICommand cmd = commandRegistry.get(command);
        response = cmd.process(args, isDiscord, discordEvent);
        return response;
    }

}
