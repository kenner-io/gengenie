package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;
import io.kenner.gengenie.cmd.DiscordBot;

import java.util.ArrayList;
import java.util.List;

/**
 * The invite command returns a link to invite the bot to a discord server.
 */
public class InviteCommand implements ICommand {
    private static final String inviteLinkFmt = "```https://discordapp.com/api/oauth2/authorize?client_id=%s&scope=bot&permissions=%s```";

    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> result = new ArrayList<String>();
        result.add(String.format(inviteLinkFmt, DiscordBot.getClientId(), DiscordBot.getPermissions()));
        return result;
    }

    @Override
    public String getHelpText() {
        return "Gets an invite link to add GenGenie to a server.";
    }

    @Override
    public String getUsageText() {
        return "invite";
    }
}
