package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;

import java.util.ArrayList;
import java.util.List;

public class DumpCommand implements ICommand {
    private static final String reactions = "reactions";

    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> result = new ArrayList<String>();
        if (args == null || args.length == 0) {
            result.add(this.getHelpText());
            return result;
        }
        String commandName = args[0].toLowerCase();
        switch (commandName) {
            case reactions:
                result.add(getReactionsDump());
            default:
                // unknown sub command, return help text
                result.add(getHelpText());
        }
        return result;
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("The dump command is used for printing troubleshooting information. Currently implemented dumps:\r\n");
        sb.append("reactions - dumps all configured reaction images to channel for inspection");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "dump [dumpType]";
    }

    /**
     * Returns a dump string of all configured reactions
     * @return
     */
    private String getReactionsDump(){
        StringBuilder sb = new StringBuilder();
        // Dump result reactions
        sb.append("Results:\r\n");
        sb.append(Reactions.SUCCESS);
        sb.append(Reactions.ADVANTAGE);
        sb.append(Reactions.FAILURE);
        sb.append(Reactions.THREAT);
        sb.append(Reactions.TRIUMPH);
        sb.append(Reactions.DESPAIR);
        sb.append("\r\n\r\n");
        // Dump dice reactions
        sb.append("Dice:\r\n");
        sb.append(Reactions.PROFICIENCY_BLANK);
        sb.append(Reactions.PROFICIENCY_S);
        sb.append(Reactions.PROFICIENCY_SS);
        sb.append(Reactions.PROFICIENCY_SA);
        sb.append(Reactions.PROFICIENCY_A);
        sb.append(Reactions.PROFICIENCY_AA);
        sb.append(Reactions.PROFICIENCY_TRI);
        sb.append(Reactions.PROFICIENCY_GIF);
        sb.append("\r\n");

        sb.append(Reactions.CHALLENGE_BLANK);
        sb.append(Reactions.CHALLENGE_F);
        sb.append(Reactions.CHALLENGE_FF);
        sb.append(Reactions.CHALLENGE_FT);
        sb.append(Reactions.CHALLENGE_T);
        sb.append(Reactions.CHALLENGE_TT);
        sb.append(Reactions.CHALLENGE_DES);
        sb.append(Reactions.CHALLENGE_GIF);
        sb.append("\r\n");

        sb.append(Reactions.ABILITY_BLANK);
        sb.append(Reactions.ABILITY_S);
        sb.append(Reactions.ABILITY_SS);
        sb.append(Reactions.ABILITY_SA);
        sb.append(Reactions.ABILITY_A);
        sb.append(Reactions.ABILITY_AA);
        sb.append(Reactions.ABILITY_GIF);
        sb.append("\r\n");

        sb.append(Reactions.DIFFICULTY_BLANK);
        sb.append(Reactions.DIFFICULTY_F);
        sb.append(Reactions.DIFFICULTY_FF);
        sb.append(Reactions.DIFFICULTY_FT);
        sb.append(Reactions.DIFFICULTY_T);
        sb.append(Reactions.DIFFICULTY_TT);
        sb.append(Reactions.DIFFICULTY_GIF);
        sb.append("\r\n");

        sb.append(Reactions.BONUS_BLANK);
        sb.append(Reactions.BONUS_S);
        sb.append(Reactions.BONUS_SA);
        sb.append(Reactions.BONUS_A);
        sb.append(Reactions.BONUS_AA);
        sb.append(Reactions.BONUS_GIF);
        sb.append("\r\n");

        sb.append(Reactions.SETBACK_BLANK);
        sb.append(Reactions.SETBACK_F);
        sb.append(Reactions.SETBACK_T);
        sb.append(Reactions.SETBACK_GIF);
        sb.append("\r\n");

        return sb.toString();
    }
}
