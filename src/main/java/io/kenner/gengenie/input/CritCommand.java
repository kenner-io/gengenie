package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.User;
import io.kenner.gengenie.crit.CritManager;
import io.kenner.gengenie.initiative.InitiativeManager;
import io.kenner.gengenie.redis.Redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CritCommand implements ICommand {
    public static final String ERR_INVALID = "Invalid usage.  Usage: %s";
    public static final String ERR_NODISCORD = "Unable to process command; unable to access discord member.";
    public static final String ERR_NOREDIS = "Unable to process command; no connection to Redis available.";

    // subcommands
    private static final String cmdMe = "me";
    private static final String cmdNpc = "npc";
    private static final String cmdGet = "get";
    private static final String cmdClearAll = "clearall";
    private static final String cmdClear = "clear";
    private static final String cmdClearOldest = "clearoldest";
    private static final String cmdLongRest = "longrest";



    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> output = new ArrayList<String>();
        if (args == null || args.length == 0) {
            output.add(getHelpText());
            return output;
        } else if (!isDiscord || discordEvent == null || discordEvent.getMember() == null) {
            output.add(ERR_NODISCORD);
            return output;
        } else if (!Redis.getConnectionStatus()) {
            output.add(ERR_NOREDIS);
            return output;
        }

        // get the discord member object
        Optional<User> om = discordEvent.getMessage().getAuthor();
        String channelId = discordEvent.getMessage().getChannelId().asString();
        User member = om.get();
        if(member == null || channelId == null || channelId.equals("")) {
            output.add(ERR_NODISCORD);
            return output;
        }

        // get the commandName
        String cmdName = args[0].trim().toLowerCase();
        if (cmdName.length() == 0) {
            output.add(String.format(ERR_INVALID, getHelpText()));
            return output;
        }
        boolean isPlayer = true;
        switch (cmdName) {
            case cmdMe:
                int plusRoll = 0;
                if(args.length > 1) {
                    try {
                        plusRoll = Integer.parseInt(args[1].trim());
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
                output.add(CritManager.addCrit(channelId, member.getId().asString(), true, plusRoll));
                break;
            case cmdNpc:
                plusRoll = 0;
                if(args.length < 2) {
                    output.add(String.format(ERR_INVALID, getHelpText()));
                    return output;
                }
                if(args.length > 2) {
                    try {
                        plusRoll = Integer.parseInt(args[2].trim());
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
                output.add(CritManager.addCrit(channelId, args[1].toLowerCase().trim(), false, plusRoll));
                break;
            case cmdGet:
                if(args.length > 1) {
                    isPlayer = false;
                }
                if(!isPlayer) {
                    output.add(CritManager.getCrits(channelId, args[1].toLowerCase().trim(), isPlayer));
                } else {
                    output.add(CritManager.getCrits(channelId, member.getId().asString(), isPlayer));
                }
                break;
            case cmdClearAll:
                if(args.length > 1) {
                    isPlayer = false;
                }
                if(!isPlayer) {
                    output.add(CritManager.clearCrits(channelId, args[1].toLowerCase().trim(), isPlayer));
                } else {
                    output.add(CritManager.clearCrits(channelId, member.getId().asString(), isPlayer));
                }
                break;
            case cmdClear:
                if(args.length < 2) {
                    output.add(String.format(ERR_INVALID, getHelpText()));
                    return output;
                }
                if(args.length > 2) {
                    isPlayer = false;
                }

                // attempt to parse index
                int index = 0;
                try {
                    index = Integer.parseInt(args[1].trim());
                } catch (Exception e) {
                    output.add("Index must be a number.");
                    return output;
                }

                if(!isPlayer){
                    output.add(CritManager.clearCritAt(channelId, args[2].trim().toLowerCase(), isPlayer, index));
                } else {
                    output.add(CritManager.clearCritAt(channelId, member.getId().asString(), isPlayer, index));
                }
                break;
            case cmdClearOldest:
                if(args.length > 1) {
                    isPlayer = false;
                }
                if(!isPlayer){
                    output.add(CritManager.clearCritAt(channelId, args[1].trim().toLowerCase(), isPlayer, 1));
                } else {
                    output.add(CritManager.clearCritAt(channelId, member.getId().asString(), isPlayer, 1));
                }
                break;
            case cmdLongRest:
                if(args.length < 2) {
                    output.add(String.format(ERR_INVALID, getHelpText()));
                    return output;
                }

                // attempt to parse index
                index = 0;
                try {
                    index = Integer.parseInt(args[1].trim());
                } catch (Exception e) {
                    output.add("Index must be a number.");
                    return output;
                }

                output.add(CritManager.healPlayerCritAt(member.getId().asString(), index));
                break;
            default:
                output.add(String.format(ERR_INVALID, getHelpText()));
                return output;
        }

        return output;
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("```");
        sb.append("crit has several subfunctions for managing player and npc crit statuses.\r\n");
        sb.append("Usage: ");
        sb.append("`"+getUsageText()+"` will get help text.\r\n");
        sb.append("`crit me [+roll]` will crit the player, rolling (1d100+[+roll]+(existingCrits*10)) and returning the appropriate critical injury.\r\n");
        sb.append("`crit npc npcId [+roll]` will crit the specified npc, rolling (1d100+[+roll]+(existingCrits*10)) and returning the appropriate critical injury.\r\n");
        sb.append("`crit get [npcId]` will get the list of critical injuries for the given character.\r\n");
        sb.append("`crit clearall [npcId]` will clear the list of critical injuries for the given character.\r\n");
        sb.append("`crit clear index [npcId]` will clear the critical injury at the given index (1, 2, etc) for the given character.\r\n");
        sb.append("`crit clearoldest [npcId]` will clear the oldest critical injury for the given character.\r\n");
        sb.append("`crit longrest index` will attempt to heal a player critical injury at the given index (1, 2, etc) using their `resilience` dice pool.\r\n");
        sb.append("```");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "crit";
    }
}
