package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.User;
import io.kenner.gengenie.dice.SkillDice;
import io.kenner.gengenie.redis.Redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SetDiceCommand implements ICommand {
    private static final String numericRegex = "-?\\d+";
    public static final String ERR_NOARGS = "Invalid usage.  Usage: %s";
    public static final String ERR_NODICE = "No dice for given pool!";
    public static final String ERR_NODISCORD = "Unable to process command; unable to access discord member.";
    public static final String ERR_NOREDIS = "Unable to process command; no connection to Redis available.";
    public static final String ERR_INVALIDKEY = "Unable to use key (%s).";
    public static final String ERR_MAXDICE = "Unable to process command; Exceeds max dice pool length of 20.";
    public static final String keyFormat = "%s.dicepool.%s";
    public static final String successfulOutputFmt = "Dice pool (%s) assigned dice (%s).";

    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> output = new ArrayList<String>();
        if (args == null || args.length == 0) {
            output.add(String.format(ERR_NOARGS, getUsageText()));
            return output;
        } else if (args.length == 1) {
            output.add(ERR_NODICE);
            return output;
        } else if (!isDiscord || discordEvent == null || discordEvent.getMember() == null) {
            output.add(ERR_NODISCORD);
            return output;
        } else if (!Redis.getConnectionStatus()) {
            output.add(ERR_NOREDIS);
            return output;
        }

        // get the discord member object
        Optional<User> om = discordEvent.getMessage().getAuthor();
        User member = om.get();
        if(member == null) {
            output.add(ERR_NODISCORD);
            return output;
        }

        // get the poolname
        String poolName = args[0].trim().toLowerCase();
        if (poolName.length() == 0) {
            output.add(String.format(ERR_INVALIDKEY, poolName));
            return output;
        }

        // get the dice
        String dice = args[1].trim().toLowerCase();
        if (dice.length() == 0) {
            output.add(ERR_NODICE);
            return output;
        } else if (dice.length() > 20) {
            output.add(ERR_MAXDICE);
            return output;
        }

        // create key for dice pool
        String key = String.format(keyFormat, member.getId().asString(), poolName);
        try {
            Redis.set(key, dice);
            String diceOutput = dice;
            if (isDiscord && !dice.matches(numericRegex)) {
                SkillDice pool = SkillDice.parse(dice);
                diceOutput = pool.getBlankDice();
            }
            output.add(String.format(successfulOutputFmt, poolName, diceOutput));
        } catch (Exception e) {
            output.add("Unable to store value: " + e.getMessage());
            e.printStackTrace();
        }

        return output;
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("```");
        sb.append("setdice allows you to assign a standard set of dice to a poolname.\r\n");
        sb.append("Usage: ");
        sb.append(getUsageText()+"\r\n");
        sb.append("Example: setdice brawl yyyg\r\n");
        sb.append("Setting dice is user specific, if two users set a key of `ranged` they will not conflict.");
        sb.append("```");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "setdice poolname dice";
    }
}
