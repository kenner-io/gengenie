package io.kenner.gengenie.input;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Reactions hosts the reaction/emoji config in a statically accessible way
 */
public class Reactions {

    // publicly accessible reactions/constants
    public static final String SUCCESS;
    public static final String ADVANTAGE;
    public static final String FAILURE;
    public static final String THREAT;
    public static final String TRIUMPH;
    public static final String DESPAIR;

    public static final String PROFICIENCY_BLANK;
    public static final String PROFICIENCY_S;
    public static final String PROFICIENCY_SS;
    public static final String PROFICIENCY_SA;
    public static final String PROFICIENCY_A;
    public static final String PROFICIENCY_AA;
    public static final String PROFICIENCY_TRI;
    public static final String PROFICIENCY_GIF;

    public static final String CHALLENGE_BLANK;
    public static final String CHALLENGE_F;
    public static final String CHALLENGE_FF;
    public static final String CHALLENGE_FT;
    public static final String CHALLENGE_T;
    public static final String CHALLENGE_TT;
    public static final String CHALLENGE_DES;
    public static final String CHALLENGE_GIF;

    public static final String ABILITY_BLANK;
    public static final String ABILITY_S;
    public static final String ABILITY_SS;
    public static final String ABILITY_SA;
    public static final String ABILITY_A;
    public static final String ABILITY_AA;
    public static final String ABILITY_GIF;

    public static final String DIFFICULTY_BLANK;
    public static final String DIFFICULTY_F;
    public static final String DIFFICULTY_FF;
    public static final String DIFFICULTY_FT;
    public static final String DIFFICULTY_T;
    public static final String DIFFICULTY_TT;
    public static final String DIFFICULTY_GIF;

    public static final String BONUS_BLANK;
    public static final String BONUS_S;
    public static final String BONUS_SA;
    public static final String BONUS_A;
    public static final String BONUS_AA;
    public static final String BONUS_GIF;

    public static final String SETBACK_BLANK;
    public static final String SETBACK_F;
    public static final String SETBACK_T;
    public static final String SETBACK_GIF;

    /**
     * Load statically assessible reaction config
     */
    private static final Properties reactionConfig;
    static {
        reactionConfig = new Properties();
        try {
            reactionConfig.load(Reactions.class.getResourceAsStream("/reactions.properties"));
        } catch (IOException e) {
            System.out.println("Error loading result reactions configuration: " + e.getMessage());
            e.printStackTrace();
            System.exit(0);
        }

        // set constants from config
        //results
        SUCCESS = reactionConfig.getProperty("result.success");
        ADVANTAGE = reactionConfig.getProperty("result.advantage");
        FAILURE = reactionConfig.getProperty("result.failure");
        THREAT = reactionConfig.getProperty("result.threat");
        TRIUMPH = reactionConfig.getProperty("result.triumph");
        DESPAIR = reactionConfig.getProperty("result.despair");

        // dice
        PROFICIENCY_BLANK = reactionConfig.getProperty("dice.proficiency.blank");
        PROFICIENCY_S = reactionConfig.getProperty("dice.proficiency.s");
        PROFICIENCY_SS = reactionConfig.getProperty("dice.proficiency.ss");
        PROFICIENCY_SA = reactionConfig.getProperty("dice.proficiency.sa");
        PROFICIENCY_A = reactionConfig.getProperty("dice.proficiency.a");
        PROFICIENCY_AA = reactionConfig.getProperty("dice.proficiency.aa");
        PROFICIENCY_TRI = reactionConfig.getProperty("dice.proficiency.tri");
        PROFICIENCY_GIF = reactionConfig.getProperty("dice.proficiency.gif");

        CHALLENGE_BLANK = reactionConfig.getProperty("dice.challenge.blank");
        CHALLENGE_F = reactionConfig.getProperty("dice.challenge.f");
        CHALLENGE_FF = reactionConfig.getProperty("dice.challenge.ff");
        CHALLENGE_FT = reactionConfig.getProperty("dice.challenge.ft");
        CHALLENGE_T = reactionConfig.getProperty("dice.challenge.t");
        CHALLENGE_TT = reactionConfig.getProperty("dice.challenge.tt");
        CHALLENGE_DES = reactionConfig.getProperty("dice.challenge.des");
        CHALLENGE_GIF = reactionConfig.getProperty("dice.challenge.gif");

        ABILITY_BLANK = reactionConfig.getProperty("dice.ability.blank");
        ABILITY_S = reactionConfig.getProperty("dice.ability.s");
        ABILITY_SS = reactionConfig.getProperty("dice.ability.ss");
        ABILITY_SA = reactionConfig.getProperty("dice.ability.sa");
        ABILITY_A = reactionConfig.getProperty("dice.ability.a");
        ABILITY_AA = reactionConfig.getProperty("dice.ability.aa");
        ABILITY_GIF = reactionConfig.getProperty("dice.ability.gif");

        DIFFICULTY_BLANK = reactionConfig.getProperty("dice.difficulty.blank");
        DIFFICULTY_F = reactionConfig.getProperty("dice.difficulty.f");
        DIFFICULTY_FF = reactionConfig.getProperty("dice.difficulty.ff");
        DIFFICULTY_FT = reactionConfig.getProperty("dice.difficulty.ft");
        DIFFICULTY_T = reactionConfig.getProperty("dice.difficulty.t");
        DIFFICULTY_TT = reactionConfig.getProperty("dice.difficulty.tt");
        DIFFICULTY_GIF = reactionConfig.getProperty("dice.difficulty.gif");

        BONUS_BLANK = reactionConfig.getProperty("dice.bonus.blank");
        BONUS_S = reactionConfig.getProperty("dice.bonus.s");
        BONUS_SA = reactionConfig.getProperty("dice.bonus.sa");
        BONUS_A = reactionConfig.getProperty("dice.bonus.a");
        BONUS_AA = reactionConfig.getProperty("dice.bonus.aa");
        BONUS_GIF = reactionConfig.getProperty("dice.bonus.gif");

        SETBACK_BLANK = reactionConfig.getProperty("dice.setback.blank");
        SETBACK_F = reactionConfig.getProperty("dice.setback.f");
        SETBACK_T = reactionConfig.getProperty("dice.setback.t");
        SETBACK_GIF = reactionConfig.getProperty("dice.setback.gif");
    }

    /**
     * Returns a reaction from the config
     * @param key
     * @return
     */
    public static String getReaction(String key) {
        return reactionConfig.getProperty(key);
    }
}
