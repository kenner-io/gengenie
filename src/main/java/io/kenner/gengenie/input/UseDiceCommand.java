package io.kenner.gengenie.input;

import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.User;
import io.kenner.gengenie.dice.Die;
import io.kenner.gengenie.dice.Result;
import io.kenner.gengenie.dice.ResultReport;
import io.kenner.gengenie.dice.SkillDice;
import io.kenner.gengenie.redis.Redis;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UseDiceCommand implements ICommand {
    private static final String dRollFormat = "D%d result: %d";
    private static final String numericRegex = "-?\\d+";
    public static final String ERR_NOARGS = "Invalid usage.  Usage: %s";
    public static final String ERR_NODICE = "No dice for given pool!";
    public static final String ERR_NODISCORD = "Unable to process command; unable to access discord member.";
    public static final String ERR_NOREDIS = "Unable to process command; no connection to Redis available.";
    public static final String ERR_INVALIDKEY = "Unable to use key (%s).";
    public static final String keyFormat = "%s.dicepool.%s";

    @Override
    public List<String> process(String[] args, boolean isDiscord, MessageCreateEvent discordEvent) {
        List<String> output = new ArrayList<String>();
        if (args == null || args.length == 0) {
            output.add(String.format(ERR_NOARGS, getUsageText()));
            return output;
        } else if (!isDiscord || discordEvent == null || discordEvent.getMember() == null) {
            output.add(ERR_NODISCORD);
            return output;
        } else if (!Redis.getConnectionStatus()) {
            output.add(ERR_NOREDIS);
            return output;
        }

        // get the discord member object
        String discordId = "";
        Optional<User> om = discordEvent.getMessage().getAuthor();
        User member = om.get();
        if(member == null) {
            output.add(ERR_NODISCORD);
            return output;
        }
        discordId = member.getId().asString();

        // get the poolname
        String poolName = args[0].trim().toLowerCase();
        if (poolName.length() == 0) {
            output.add(String.format(ERR_INVALIDKEY, poolName));
            return output;
        }

        // get the additional dice
        String addlDice = "";
        if (args.length > 1) {
            addlDice = args[1].trim().toLowerCase();
        }

        // retrieve key for dice pool
        String key = String.format(keyFormat, member.getId().asString(), poolName);
        String savedDice = "";
        try {
            savedDice = Redis.get(key);
        } catch (Exception e) {
            output.add("Unable to retrieve value: " + e.getMessage());
            e.printStackTrace();
        }

        String allDice = savedDice+addlDice;
        boolean isNumericRoll = allDice.matches(numericRegex);
        if(isNumericRoll) {
            try {
                int sides = Math.abs(Integer.parseInt(allDice));
                int rawRoll = Die.RollD(sides);
                output.add(String.format(dRollFormat, sides, rawRoll));
            } catch (Exception e) {
                output.add("Error parsing sides");
            }
            return output;
        }
        SkillDice diceBag = SkillDice.parse(allDice);
        return diceBag.rollWithOutput(isDiscord, discordId);
    }

    @Override
    public String getHelpText() {
        StringBuilder sb = new StringBuilder();
        sb.append("```");
        sb.append("usedice allows you to roll a dice pool created using setdice, plus additional dice.\r\n");
        sb.append("Usage: ");
        sb.append(getUsageText()+"\r\n");
        sb.append("Example: usedice brawl pp\r\n");
        sb.append("Dice are user specific, if two users have a key of `ranged` they will not conflict.");
        sb.append("```");
        return sb.toString();
    }

    @Override
    public String getUsageText() {
        return "usedice poolname [additional dice]";
    }
}
