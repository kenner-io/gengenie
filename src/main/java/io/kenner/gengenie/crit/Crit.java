package io.kenner.gengenie.crit;

import io.kenner.gengenie.dice.SkillDice;
import io.kenner.gengenie.input.Reactions;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement(name = "crit")
@XmlAccessorType(XmlAccessType.FIELD)
public class Crit implements Serializable {
    private int roll;
    private String severity;
    private String name;
    private String description;

    @XmlTransient
    private static final Map<CritKey, Crit> crits = new HashMap<CritKey, Crit>();
    static {
        // populate the crits map on startup.
        crits.put(new CritKey(5), new Crit(0, "p", "Minor Nick", "The target suffers 1 strain."));
        crits.put(new CritKey(10), new Crit(0, "p", "Slowed Down", "The target can only act during the last allied Initiative slot on their next turn."));
        crits.put(new CritKey(15), new Crit(0, "p", "Sudden Jolt", "The target drops whatever is in hand."));
        crits.put(new CritKey(20), new Crit(0, "p", "Distracted", "The target cannot perform a free maneuver during their next turn."));
        crits.put(new CritKey(25), new Crit(0, "p", "Off-Balance", "Add "+ Reactions.SETBACK_BLANK+" to the target's next skill check."));
        crits.put(new CritKey(30), new Crit(0, "p", "Discouraging Wound", "Move one player pool Story Point to the Game Master pool (reverse if NPC)."));
        crits.put(new CritKey(35), new Crit(0, "p", "Stunned", "The target is staggered until the end of their next turn."));
        crits.put(new CritKey(40), new Crit(0, "p", "Stinger", "Increase the difficulty of the target’s next check by one."));
        crits.put(new CritKey(45), new Crit(0, "pp", "Bowled Over", "The target is knocked prone and suffers 1 strain."));
        crits.put(new CritKey(50), new Crit(0, "pp", "Head Ringer", "The target increases the difficulty of all Intellect and Cunning checks by one until this Critical Injury is healed."));
        crits.put(new CritKey(55), new Crit(0, "pp", "Fearsome Wound", "The target increases the difficulty of all Presence and Willpower checks by one until this Critical Injury is healed."));
        crits.put(new CritKey(60), new Crit(0, "pp", "Agonizing Wound", "The target increases the difficulty of all Brawn and Agility checks by one until this Critical Injury is healed."));
        crits.put(new CritKey(65), new Crit(0, "pp", "Slightly Dazed", "The target is disoriented until this Critical Injury is healed."));
        crits.put(new CritKey(70), new Crit(0, "pp", "Scattered Senses", "The target removes all "+Reactions.BONUS_BLANK+" from skill checks until this Critical Injury is healed."));
        crits.put(new CritKey(75), new Crit(0, "pp", "Hamstrung", "The target loses their free maneuver until this Critical Injury is healed."));
        crits.put(new CritKey(80), new Crit(0, "pp", "Overpowered", "The target leaves themself open, and the attacker may immediately attempt another attack against them as an incidental, using the exact same pool as the original attack."));
        crits.put(new CritKey(85), new Crit(0, "pp", "Winded", "The target cannot voluntarily suffer strain to activate any abilities or gain additional maneuvers until this Critical Injury is healed."));
        crits.put(new CritKey(90), new Crit(0, "pp", "Compromised", "Increase difficulty of all skill checks by one until this Critical Injury is healed."));
        crits.put(new CritKey(95), new Crit(0, "pp", "At the Brink", "The target suffers 2 strain each time they perform an action until this Critical Injury is healed."));
        crits.put(new CritKey(100), new Crit(0, "ppp", "Crippled", "One of the target’s limbs (selected by the GM) is impaired until this Critical Injury is healed. Increase difficulty of all checks that require use of that limb by one."));
        crits.put(new CritKey(105), new Crit(0, "ppp", "Maimed", "One of the target’s limbs (selected by the GM) is permanently lost. Unless the target has a cybernetic or prosthetic replacement, the target cannot perform actions that would require the use of that limb. All other actions gain "+Reactions.SETBACK_BLANK+" until this Critical Injury is healed."));
        crits.put(new CritKey(110), new Crit(0, "ppp", "Horrific Injury", "Roll 1d10 to determine which of the target’s characteristics is affected: 1–3 for Brawn, 4–6 for Agility, 7 for Intellect, 8 for Cunning, 9 for Presence, 10 for Willpower. Until this Critical Injury is healed, treat that characteristic as one point lower."));
        crits.put(new CritKey(115), new Crit(0, "ppp", "Temporarily Disabled", "The target is immobilized until this Critical Injury is healed."));
        crits.put(new CritKey(120), new Crit(0, "ppp", "Blinded", "The target can no longer see. Upgrade the difficulty of all checks twice, and upgrade the difficulty of Perception and Vigilance checks three times, until this Critical Injury is healed."));
        crits.put(new CritKey(125), new Crit(0, "ppp", "Knocked Senseless", "The target is staggered until this Critical Injury is healed."));
        crits.put(new CritKey(130), new Crit(0, "pppp", "Gruesome Injury", "Roll 1d10 to determine which of the target’s characteristics is affected: 1–3 for Brawn, 4–6 for Agility, 7 for Intellect, 8 for Cunning, 9 for Presence, 10 for Willpower. That characteristic is permanently reduced by one, to a minimum of 1."));
        crits.put(new CritKey(140), new Crit(0, "pppp", "Bleeding Out", "Until this Critical Injury is healed, every round, the target suffers 1 wound and 1 strain at the beginning of their turn. For every 5 wounds they suffer beyond their wound threshold, they suffer one additional Critical Injury. Roll on the chart, suffering the injury (if they suffer this result a second time due to this, roll again)."));
        crits.put(new CritKey(150), new Crit(0, "pppp", "The End is Nigh", "The target dies after the last Initiative slot during the next round unless this Critical Injury is healed."));
        crits.put(new CritKey(151), new Crit(0, "", "Dead", "Complete, obliterated death."));
    }

    public Crit() {
        this.roll = 0;
        this.severity = "";
        this.name = "";
        this.description = "";
    }

    public Crit(int roll, String severity, String name, String description) {
        this.roll = roll;
        this.severity = severity;
        this.name = name;
        this.description = description;
    }

    public Crit(int roll) {
        this.roll = roll;
        CritKey key = new CritKey(roll);
        Crit copyFrom = Crit.crits.get(key);
        this.severity = copyFrom.severity;
        this.name = copyFrom.name;
        this.description = copyFrom.description;
    }

    public String toDiscordString() {
        StringBuilder sb = new StringBuilder();
        SkillDice sevPool = SkillDice.parse(this.severity);
        sb.append(String.format("(%3d) **%s** (%s): %s", this.roll, this.name, sevPool.getBlankDice(), this.description));
        return sb.toString();
    }

    /* Getters / setters */
    public int getRoll() {
        return roll;
    }

    public void setRoll(int roll) {
        this.roll = roll;
    }

    public String getSeverity() {
        return severity;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

/* anon class for keying the crit map */
@XmlTransient
class CritKey {
    int roll;

    public CritKey(int roll) {
        this.roll = roll;
    }

    @Override
    public boolean equals(Object o) {
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;

        CritKey that = (CritKey) o;
        // field comparison
        return this.hashCode() == that.hashCode();
    }

    public int hashCode() {
        if(this.roll <= 0) {
            return 1;
        } else if(this.roll <= 130) {
            return this.roll / 5;
        } else if(this.roll <= 140){
            return 28;
        } else if(this.roll <= 150) {
            return 29;
        } else {
            return 30;
        }
    }
}
