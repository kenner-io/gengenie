package io.kenner.gengenie.crit;

import io.kenner.gengenie.initiative.Initiative;
import io.kenner.gengenie.redis.Redis;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "playerCrits")
@XmlAccessorType(XmlAccessType.FIELD)
public class PlayerCrits implements Serializable {
    public static final String keyFmt = "player.crits.%s";

    private String id;
    private List<Integer> critRolls;
    @XmlTransient
    private List<Crit> crits;

    public PlayerCrits() {
        this.id = "";
        this.critRolls = new ArrayList<Integer>();
        this.crits = new ArrayList<Crit>();
    }

    public PlayerCrits(String id) {
        this.id = id;
        this.critRolls = new ArrayList<Integer>();
        this.crits = new ArrayList<Crit>();
    }

    /**
     * Attempts to write to Redis.  Returns a string error if one occurs.
     * @return
     */
    protected String write() {
        try {
            Redis.set(String.format(keyFmt, this.id), this.toJsonString());
        } catch (Exception e) {
            e.printStackTrace();
            return "Unable to update player tracked crits.";
        }
        return "";
    }

    /**
     * Attempts to read from Redis.  If record doesn't exist, returns basic object.
     * @param playerId
     * @return
     */
    public static PlayerCrits getOrCreate(String playerId) {
        PlayerCrits obj = new PlayerCrits(playerId);
        String redisKey = String.format(keyFmt, playerId);
        String redisValue = "";
        try {
            redisValue = Redis.get(redisKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(redisValue != null) {
            PlayerCrits storedResult = PlayerCrits.fromJsonString(redisValue);
            if(storedResult != null) {
                if (storedResult.getCritRolls() != null) {
                    for(int critRoll : storedResult.getCritRolls()) {
                        Crit crit = new Crit(critRoll);
                        storedResult.getCrits().add(crit);
                    }
                }
                return storedResult;
            }
        }
        // unable to get value from Redis, return base object.
        return obj;
    }

    /**
     * Attempts to unmarshall from a json string.  Returns null on failure
     * @param json
     * @return
     */
    public static PlayerCrits fromJsonString(String json) {
        JAXBContext jaxbContext;
        try {
            jaxbContext = JAXBContext.newInstance(PlayerCrits.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
            jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

            PlayerCrits obj = (PlayerCrits)jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(json.getBytes(StandardCharsets.UTF_8)));
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Unmarshalling failed
        return null;
    }

    /**
     * Attempts to marshall into a json string.  Returns empty string on failure
     * @return
     */
    public String toJsonString() throws Exception{
        String json = "";
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(PlayerCrits.class);

            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
            jaxbMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

            //Print JSON String to Console
            StringWriter sw = new StringWriter();
            jaxbMarshaller.marshal(this, sw);
            //todo delete
            System.out.println(sw.toString());
            json = sw.toString();
        } catch (Exception e) {
            throw e;
        }

        return json;
    }

    /** Getters / Setters **/
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Crit> getCrits() {
        return crits;
    }

    public void setCrits(List<Crit> crits) {
        this.crits = crits;
    }

    public List<Integer> getCritRolls() {
        return critRolls;
    }

    public void setCritRolls(List<Integer> critRolls) {
        this.critRolls = critRolls;
    }
}
