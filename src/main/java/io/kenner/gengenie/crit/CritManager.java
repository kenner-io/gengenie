package io.kenner.gengenie.crit;

import io.kenner.gengenie.dice.Die;
import io.kenner.gengenie.dice.ResultReport;
import io.kenner.gengenie.dice.SkillDice;
import io.kenner.gengenie.initiative.InitCharacter;
import io.kenner.gengenie.initiative.Initiative;
import io.kenner.gengenie.input.SetDiceCommand;
import io.kenner.gengenie.redis.Redis;

import java.util.ArrayList;

public class CritManager {
    private static final String missingStatsErr = "<@%s> you must set your resilience dice pools using `setdice` before attempting to rest recover from a critical injury..";

    public static String addCrit(String channelId, String id, boolean isPlayer, int plusRoll) {
        if(isPlayer) {
            // get any existing record or make a clean one
            PlayerCrits crits = PlayerCrits.getOrCreate(id);
            // if there are any existing crits, add that to the +Roll
            plusRoll += crits.getCrits().size()*10;
            // roll the d100
            int roll = Die.RollD(100) + plusRoll;
            Crit newCrit = new Crit(roll);
            // save the new crit
            crits.getCritRolls().add(roll);
            crits.getCrits().add(newCrit);
            String writeErr = crits.write();
            if(!writeErr.isEmpty()) {
                return writeErr;
            }
            return String.format("<@%s>: %s", id, newCrit.toDiscordString());
        } else {
            // get the channel initiative report
            Initiative init = Initiative.getOrCreate(channelId);
            if (init.getNpcs() != null) {
                for (InitCharacter npc : init.getNpcs()) {
                    if(npc.getId().equalsIgnoreCase(id)) {
                        // if there are any existing crits, add that to the +Roll
                        if(npc.getCrits() == null) {
                            npc.setCrits(new ArrayList<Crit>());
                        }
                        plusRoll += npc.getCrits().size()*10;
                        // roll the d100
                        int roll = Die.RollD(100) + plusRoll;

                        // save the new crit
                        if(npc.getCritRolls() == null) {
                            npc.setCritRolls(new ArrayList<Integer>());
                        }
                        npc.getCritRolls().add(roll);
                        Crit newCrit = new Crit(roll);
                        npc.getCrits().add(newCrit);

                        String writeErr = init.write();
                        if(!writeErr.isEmpty()) {
                            return writeErr;
                        }

                        // return the crit
                        return String.format("%s: %s", id, newCrit.toDiscordString());
                    }
                }
            }

            return String.format("Unregistered npc (%s).  Register using `init addnpc name cool vigilance`.  See registered npcs using `init report`",id);
        }
    }

    public static String getCrits(String channelId, String id, boolean isPlayer) {
        if(isPlayer) {
            // get any existing record or make a clean one
            PlayerCrits crits = PlayerCrits.getOrCreate(id);
            if(crits.getCrits() == null || crits.getCrits().isEmpty()) {
                return String.format("<@%s>: %s", id, "Not suffering from any critical injuries.");
            }
            StringBuilder sb = new StringBuilder();
            sb.append(String.format("<@%s> is suffering from the following critical injuries:\r\n", id));
            for(Crit crit : crits.getCrits()) {
                sb.append(crit.toDiscordString() + "\r\n");
            }
            return sb.toString();
        } else {
            // get the channel initiative report
            Initiative init = Initiative.getOrCreate(channelId);
            if (init.getNpcs() != null) {
                for (InitCharacter npc : init.getNpcs()) {
                    if(npc.getId().equalsIgnoreCase(id)) {
                        // if there are any existing crits, add that to the +Roll
                        if(npc.getCrits() == null || npc.getCrits().isEmpty()) {
                            return String.format("%s: %s", id, "Not suffering from any critical injuries.");
                        }
                        StringBuilder sb = new StringBuilder();
                        sb.append(String.format("%s is suffering from the following critical injuries:\r\n", id));
                        for(Crit crit : npc.getCrits()) {
                            sb.append(crit.toDiscordString() + "\r\n");
                        }
                        return sb.toString();
                    }
                }
            }

            return String.format("Unregistered npc (%s).  Register using `init addnpc name cool vigilance`.  See registered npcs using `init report`",id);
        }
    }

    public static String clearCrits(String channelId, String id, boolean isPlayer) {
        if(isPlayer) {
            // get any existing record or make a clean one
            PlayerCrits crits = PlayerCrits.getOrCreate(id);
            // reset the crits list
            crits.setCritRolls(new ArrayList<Integer>());
            crits.setCrits(new ArrayList<Crit>());
            // write the update
            String writeErr = crits.write();
            if(!writeErr.isEmpty()) {
                return writeErr;
            } else {
                return String.format("<@%s>: All critical injuries healed.", id);
            }
        } else {
            // get the channel initiative report
            Initiative init = Initiative.getOrCreate(channelId);
            if (init.getNpcs() != null) {
                for (InitCharacter npc : init.getNpcs()) {
                    if (npc.getId().equalsIgnoreCase(id)) {
                        npc.setCritRolls(new ArrayList<Integer>());
                        npc.setCrits(new ArrayList<Crit>());
                        // update the init
                        String writeErr = init.write();
                        if(!writeErr.isEmpty()) {
                            return writeErr;
                        } else {
                            return String.format("%s: All critical injuries healed.", id);
                        }
                    }
                }
            }

            return String.format("Unregistered npc (%s).  Register using `init addnpc name cool vigilance`.  See registered npcs using `init report`",id);
        }
    }

    public static String clearCritAt(String channelId, String id, boolean isPlayer, int index) {
        // we take base 1 input
        if(index < 1) {
            return "Critical Injury index number must be 1 or greater.";
        }
        // decrement to base 0
        index--;
        if(isPlayer) {
            // get any existing record or make a clean one
            PlayerCrits crits = PlayerCrits.getOrCreate(id);
            if(crits.getCrits() == null || crits.getCrits().isEmpty() || crits.getCrits().size() < index) {
                return String.format("<@%s>: There is no critical injury at index %d to heal.", id, index+1);
            }

            // reset the crits list
            crits.getCrits().remove(index);
            crits.getCritRolls().remove(index);
            // write the update
            String writeErr = crits.write();
            if(!writeErr.isEmpty()) {
                return writeErr;
            } else {
                return String.format("<@%s>: Healed critical injury #%d.", id, index+1);
            }
        } else {
            // get the channel initiative report
            Initiative init = Initiative.getOrCreate(channelId);
            if (init.getNpcs() != null) {
                for (InitCharacter npc : init.getNpcs()) {
                    if (npc.getId().equalsIgnoreCase(id)) {
                        if(npc.getCrits() == null || npc.getCrits().isEmpty() || npc.getCrits().size() < index) {
                            return String.format("%s: There is no critical injury at index %d to heal.", id, index+1);
                        }
                        npc.getCritRolls().remove(index);
                        npc.getCrits().remove(index);
                        // update the init
                        String writeErr = init.write();
                        if(!writeErr.isEmpty()) {
                            return writeErr;
                        } else {
                            return String.format("%s: Healed critical injury #%d.", id, index+1);
                        }
                    }
                }
            }

            return String.format("Unregistered npc (%s).  Register using `init addnpc name cool vigilance`.  See registered npcs using `init report`",id);
        }
    }

    public static String healPlayerCritAt(String id, int index) {
        // we take base 1 input
        if(index < 1) {
            return "Critical Injury index number must be 1 or greater.";
        }
        // decrement to base 0
        index--;
        // get the player resilience pool (or return an error they must set it)
        String resilience = "";
        try {
            resilience = Redis.get(String.format(SetDiceCommand.keyFormat, id, "resilience"));
        } catch (Exception e) {
            e.printStackTrace();
            return String.format("<@%s>: Issue fetching your resilience dice pool.", id);
        }
        if(resilience == null || resilience.isEmpty()) {
            return String.format(missingStatsErr, id);
        }

        // get any existing record or make a clean one
        PlayerCrits crits = PlayerCrits.getOrCreate(id);
        if(crits.getCrits() == null || crits.getCrits().isEmpty()|| crits.getCrits().size() < index) {
            return String.format("<@%s>: There is no critical injury at index %d to heal.", id, index+1);
        }

        // reset the crits list
        // get the crit they're trying to heal
        Crit crit = crits.getCrits().get(index);
        SkillDice healRoll = SkillDice.parse(resilience+crit.getSeverity());
        ResultReport report = healRoll.roll();

        StringBuilder output = new StringBuilder();
        if(report.getNetResult().getSuccesses() > 0 || report.getNetResult().getTriumphs() > 0) {
            // remove the crit injury
            crits.getCrits().remove(index);
            crits.getCritRolls().remove(index);
            ArrayList<Crit> autoHealedCrits = new ArrayList<Crit>();
            int remainingHeals = 0;
            if(report.getNetResult().getTriumphs() > 0) {
                if(report.getNetResult().getTriumphs() >= crits.getCritRolls().size()) {
                    // Enough triumphs to heal all remaining crit injuires
                    // track the auto healed injuries
                    autoHealedCrits.addAll(crits.getCrits());
                    // remove the crits from the player
                    crits.setCritRolls(new ArrayList<Integer>());
                    crits.setCrits(new ArrayList<Crit>());
                } else {
                    // there's more critical injuries remaining than triumphs.  We cannot auto heal, it's up to the player.
                    remainingHeals = report.getNetResult().getTriumphs();
                }
            }

            // update playerCrits in Redis
            String writeErr = crits.write();
            if(!writeErr.isEmpty()) {
                return writeErr;
            }

            // successfully completed all Redis I/O without error, construct response.
            output.append(String.format("<@%s> successfully recovered from critical injury #%d during their long rest:\r\n", id, index+1));
            output.append(crit.toDiscordString()+"\r\n");
            output.append(report.getDiceReactions()+"\r\n");
            output.append(report.getNetResult().toDiscordString()+"\r\n");
            if(report.getNetResult().getTriumphs() > 0) {
                output.append(String.format("In addition, they get to heal %d extra critical injuries.\r\n", report.getNetResult().getTriumphs()));
                for(Crit c : autoHealedCrits) {
                    output.append(String.format("Automatically healed:\r\n %s\r\n", crit.toDiscordString()));
                }
                if(remainingHeals > 0) {
                    output.append(String.format("Unable to automatically heal remaining crits (less triumphs than remaining crits).  Please choose (%d) crits to heal using `.gg crit clear index`:\r\n", remainingHeals));
                    for(int i = 0; i < crits.getCrits().size(); i++) {
                        output.append(String.format("#%d: %s\r\n", (i+1), crits.getCrits().get(i).toDiscordString()));
                    }
                }
            }
        } else {
            // failed to heal the critical injury, return message
            output.append(String.format("<@%s> failed to recover from critical injury #%d during their long rest:\r\n", id, index+1));
            output.append(crit.toDiscordString()+"\r\n");
            output.append(report.getDiceReactions()+"\r\n");
            output.append(report.getNetResult().toDiscordString()+"\r\n");
        }

        return output.toString();
    }
}
