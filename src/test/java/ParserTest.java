import io.kenner.gengenie.input.Parser;
import org.junit.Assert;
import org.junit.Test;

public class ParserTest {

    @Test
    public void testInvalidInput() {
        String noOutput = "";
        String case1 = "gg something or other";
        String case2 = "nah this is completely unrelated";
        String case3 = "http://something.net";
        String case4 = ".gg";
        /*String output1 = Parser.Process(case1,false);
        String output2 = Parser.Process(case2,false);
        String output3 = Parser.Process(case3,false);
        String output4 = Parser.Process(case4,false);
        Assert.assertEquals(noOutput, output1);
        Assert.assertEquals(noOutput, output2);
        Assert.assertEquals(noOutput, output3);
        Assert.assertEquals(Parser.ERR_HELP, output4);*/
    }

    @Test
    public void testCommandInput() {
        String case1 = ".gg nubcake";
        String case2 = ".gg help";  // TODO
        /*String output1 = Parser.Process(case1,false);
        Assert.assertEquals(Parser.ERR_HELP, output1);*/
    }
}
