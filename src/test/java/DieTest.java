import io.kenner.gengenie.dice.Result;
import io.kenner.gengenie.dice.ResultSets;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class DieTest {

    @Test
    public void testNetResult() {
        // Case 1:  3s 2a 0tri - 1f 3t 1des = 2s 1t 0tri 1des
        Result case1 = new Result(3, 1, 2, 3, 0, 1, 0).getNetResult();
        final Result expected1 = new Result(1, 0, 0, 1, 0, 1, 0);
        Assert.assertEquals(expected1.toString(), case1.toString());

        // Case 2:  0s 0a 1tri - 3f 2t 1des = 3f 2t 1tri 1des
        Result case2 = new Result(0, 3, 0, 2, 1, 1, 0).getNetResult();
        final Result expected2 = new Result(0, 3, 0, 2, 1, 1, 0);
        Assert.assertEquals(expected2.toString(), case2.toString());

        // Case 3:  1s 1a 0tri - 0f 0t 0des = 1s 1a 0tri 0des
        Result case3 = new Result(1, 0, 1, 0, 0, 0, 0).getNetResult();
        final Result expected3 = new Result(1, 0, 1, 0, 0, 0, 0);
        Assert.assertEquals(expected3.toString(), case3.toString());

        // Case 4:  net result of all possible dice rolls
        // expected:  2s 1a 1tri 1des
        final Result expected4 = new Result(2, 0, 1, 0, 1, 1, 0);
        // Add all possible results to a consolidated list for accumulation
        ArrayList<Result> allResults = new ArrayList<Result>();
        allResults.addAll(ResultSets.getProficiencyResults());
        allResults.addAll(ResultSets.getChallengeResults());
        allResults.addAll(ResultSets.getAbilityResults());
        allResults.addAll(ResultSets.getDifficultyResults());
        allResults.addAll(ResultSets.getBonusResults());
        allResults.addAll(ResultSets.getSetbackResults());
        Result accumResult = Result.AccumulateResults(allResults);

        Assert.assertEquals(expected4.toString(), accumResult.getNetResult().toString());
    }

    @Test
    public void testAccumResult() {
        // Add every possible die roll together.  The result should be 16s 17a 1tri 14f 16t 1des
        final Result expectedResult = new Result(16, 14, 17, 16, 1, 1, 0);

        // Add all possible results to a consolidated list for accumulation
        ArrayList<Result> allResults = new ArrayList<Result>();
        allResults.addAll(ResultSets.getProficiencyResults());
        allResults.addAll(ResultSets.getChallengeResults());
        allResults.addAll(ResultSets.getAbilityResults());
        allResults.addAll(ResultSets.getDifficultyResults());
        allResults.addAll(ResultSets.getBonusResults());
        allResults.addAll(ResultSets.getSetbackResults());

        // accumulate results and assert test
        Result accumResult = Result.AccumulateResults(allResults);
        Assert.assertEquals(expectedResult.toString(), accumResult.toString());
    }
}
