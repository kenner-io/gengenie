import io.kenner.gengenie.input.HelpCommand;
import io.kenner.gengenie.input.Parser;
import io.kenner.gengenie.input.RollCommand;
import org.junit.Assert;
import org.junit.Test;

public class CommandTest {

    @Test
    public void testHelpCommand() {
        HelpCommand help = new HelpCommand();
        RollCommand roll = new RollCommand();
        String case1 = ".gg help";
        String case2 = ".gg help invalidCommand";
        String case3 = ".gg help help";
        String case4 = ".gg hElP hELP";
        String case5 = ".gg help roll";
        String expected1 = help.getHelpText();
        String expected2 = Parser.ERR_HELP;
        String expected3 = help.getHelpText();
        String expected4 = help.getHelpText();
        String expected5 = roll.getHelpText();
        /*String output1 = Parser.Process(case1,false);
        String output2 = Parser.Process(case2,false);
        String output3 = Parser.Process(case3,false);
        String output4 = Parser.Process(case4,false);
        String output5 = Parser.Process(case5,false);
        Assert.assertEquals(expected1, output1);
        Assert.assertEquals(expected2, output2);
        Assert.assertEquals(expected3, output3);
        Assert.assertEquals(expected4, output4);
        Assert.assertEquals(expected5, output5);*/
    }

    @Test
    public void testRollCommand() {

    }
}
